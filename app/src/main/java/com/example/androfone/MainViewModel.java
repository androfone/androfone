package com.example.androfone;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    private MutableLiveData<Boolean> mMainEditMode = new MutableLiveData<>();

    public MainViewModel() {
        super();
    }

    public void setEditMode(boolean isEditMode) {
        mMainEditMode.setValue(isEditMode);
    }

    public LiveData<Boolean> getEditMode() {
        return  mMainEditMode;
    }
}
