package com.example.androfone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.androfone.filedialog.MainFileDialogFragment;
import com.example.androfone.filedialog.SoundItem;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class MainActivity extends AppCompatActivity implements SlidingMenuLayout.OnToolSelectedListener,
MainFileDialogFragment.OnFragmentResultListener {

    private static final String DEBUG_TAG = "MainActivity";

    private static int SAMPLE_RATE = 48000;
    private static int PREF_BUFFER_SIZE = 192;

    private static final String CUR_FRAG_KEY = "curFrag";
    private static final String CUR_TAB_KEY = "curTab";

    // Static variables for the SelectorTabLayout popupmenu listener switch since Google will
    // prevent the use of non final variables in switch statement in future version (5) of gradle.
    // If it cause problems, convert the switch to cascading if's statement
    // and use the id's from the menu item directly instead of those.
    private static final int BANK = R.id.sound_bank;
    private static final int RECORD = R.id.record;
    private static final int SAVE = R.id.save_fragment;
    private static final int CLEAR = R.id.clear;
    SelectorTabLayout mSessionTab;


    private FragmentManager mFragmentManager;
    private String[] mTags = new String[]{"MF1", "MF2", "MF3"}; // Hard coded session fragment tag
    private TabLayout mTab;

    public MainViewModel mMainViewModel;
    public SlidingMenuLayout mSlidingMenu;
    private boolean isMenuOpen;

    public static int getPrefBufferSize() { return PREF_BUFFER_SIZE; }
    public static int getSampleRate() { return SAMPLE_RATE; }

    private EditableWaveformView mWaveformView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        mWaveformView = findViewById(R.id.waveformview);

        mSessionTab = findViewById(R.id.session_tab_layout);
        mSessionTab.setOnSelectorTabActionListener(new SelectorTabLayout.OnSelectorTabActionListener() {
            @Override
            public void onTabSelected(int pos) {
                Log.e(DEBUG_TAG,"onTabSelected(mPosition): " + pos);
                String tag = mTags[pos];
                if (!((SelectorTabLayout.SampleTab) mSessionTab.getChildAt(pos)).isLoaded()) {
                    Log.e(DEBUG_TAG,"tab is not loaded, call dialog ");
                    MainFileDialogFragment frag = MainFileDialogFragment.newInstance(MainFileDialogFragment.START_FROM_OPENFILE, tag);
                    frag.show(getSupportFragmentManager(), "dialog");
//                    mSessionTab.loadSample("new sample");
                } else {
                    Log.e(DEBUG_TAG,"tab is loaded, just switch fragment ");
                    WorkspaceFragment frag = (WorkspaceFragment) mFragmentManager.findFragmentByTag(tag);
                    setMainView(frag.getCurSoundItem());
                }

                Log.e("MainActivity", "tag: " + tag);
                replaceFragment(tag);
            }

            @Override
            public void onTabPopupItemSelected(int pos, MenuItem item) {
                String tag = mTags[pos];
                int id = item.getItemId();
                switch (id) {
                    case BANK:
                        Log.e(DEBUG_TAG,"onTabPopupItemSelected(pos: " + pos + " sound_bank)");
                        MainFileDialogFragment frag = MainFileDialogFragment.newInstance(MainFileDialogFragment.START_FROM_OPENFILE, tag);
                        frag.show(getSupportFragmentManager(), "dialog");
//                        mSessionTab.loadSample("another sample");
                        break;
                    case RECORD:
                        Log.e(DEBUG_TAG,"onTabPopupItemSelected(pos: " + pos + " record)");
                        frag = MainFileDialogFragment.newInstance(MainFileDialogFragment.START_FROM_RECORD, tag);
                        frag.show(getSupportFragmentManager(), "dialog");
//                        mSessionTab.loadSample("recorded sample");
                        break;
                    case SAVE:
                        Log.e(DEBUG_TAG,"onTabPopupItemSelected(pos: " + pos + " save_fragment)");
                        break;
                    case CLEAR:
                        Log.e(DEBUG_TAG,"onTabPopupItemSelected(pos: " + pos + " clear)");
                        WorkspaceFragment wfrag = (WorkspaceFragment) mFragmentManager.findFragmentByTag(tag);
                        wfrag.setCurSoundItem(null);
                        setMainView(null);
                        mSessionTab.clearSample();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onTabPlayButtonClicked(int pos, boolean isPlaying) {
                // here pos represent the fragment that will receive the value
                // buttons[pos].setChecked(isPlaying);
            }
        });


//        mMainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mSlidingMenu = findViewById(R.id.sliding_menu);
        isMenuOpen = false;
        Log.e("MainActivity", " * * * * * * * * slidingMenu: " + mSlidingMenu);
//        mMainViewModel.setEditMode(isMenuOpen);
        mSlidingMenu.mHeader.setOnClickListener(v -> {
            isMenuOpen = !isMenuOpen;
            mSlidingMenu.setOpened(isMenuOpen);
//            mMainViewModel.setEditMode(isMenuOpen);
            Fragment f = mFragmentManager.findFragmentById(R.id.workspace_container);
            ((WorkspaceFragment)f).setEditMode(isMenuOpen);
        });
        mSlidingMenu.setOnToolSelectedListener(this);

        Fragment f;
        if (savedInstanceState == null) {
            f = new WorkspaceFragment();
            mFragmentManager.beginTransaction().add(R.id.workspace_container,
                    f, mTags[0]).commit();


            Log.e("MainActivity", "onCreate() fragments: " + getSupportFragmentManager().getFragments());

        } else {
            int fid = savedInstanceState.getInt(CUR_FRAG_KEY);
            f = mFragmentManager.findFragmentById(fid); // ??????? pourquoi?
//            ((WorkspaceFragment)f).setUpToolMenuListener(mSlidingMenu); // pour ça!
//            int tabPos = savedInstanceState.getInt(CUR_TAB_KEY, 0);
//            mTab.getTabAt(tabPos).select();
        }

    }

    private void replaceFragment(String tag) {
        // Switching fragment; get out of editMode
        if (isMenuOpen) isMenuOpen = !isMenuOpen;
        mSlidingMenu.setOpened(isMenuOpen);
//        mMainViewModel.setEditMode(isMenuOpen);
        Fragment currentFragment = mFragmentManager.findFragmentById(R.id.workspace_container);
        ((WorkspaceFragment)currentFragment).setEditMode(false); // Make sure to exit editMode
        Log.e("MainActivity", "replaceFragment() curfragments: " + currentFragment);
        Fragment newFragment = mFragmentManager.findFragmentByTag(tag);
        Log.e("MainActivity", "replaceFragment() newfragments: " + newFragment);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();


        transaction.detach(currentFragment);


        Log.e("MainActivity", "replaceFragment() curfragments detached");
        if (newFragment == null) {
            newFragment = new WorkspaceFragment();
            Log.e("MainActivity", "replaceFragment() newfragments created: " + newFragment);
            newFragment.setRetainInstance(true);
            transaction.add(R.id.workspace_container, newFragment, tag);
            Log.e("MainActivity", "replaceFragment() newfragments added: " + newFragment);
            transaction.setMaxLifecycle(newFragment, Lifecycle.State.STARTED);
        } else {
            transaction.attach(newFragment);
            Log.e("MainActivity", "replaceFragment() newfragments attached: " + newFragment);
        }

        transaction.commit();
        Log.e("MainActivity", "replaceFragment() commit: " + getSupportFragmentManager().getFragments());
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        Fragment f = mFragmentManager.findFragmentById(R.id.workspace_container);
        outState.putInt(CUR_FRAG_KEY, f.getId());
//        outState.putInt(CUR_TAB_KEY, mTab.getSelectedTabPosition());
        Log.e("MainActivity", "onSaveInstanceState() curFragments: " + f);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onToolSelected(View v) {
        WorkspaceFragment f = (WorkspaceFragment)mFragmentManager.findFragmentById(R.id.workspace_container);
        f.onToolSelected(v);
    }

    // This is the main listener to the file dialog result
    @Override
    public void onResult(SoundItem item, String tag) {
        Log.d(DEBUG_TAG, "onResult(SoundItem item, String tag) item: " + item.getName());
        WorkspaceFragment frag = (WorkspaceFragment) mFragmentManager.findFragmentByTag(tag);
        frag.setCurSoundItem(item);
        mSessionTab.loadSample(item.getName());
        setMainView(item);
    }

    private void setMainView(@Nullable SoundItem item) {
        Log.d(DEBUG_TAG, "setMainView(SoundItem item)");
        if (item == null) {
            short[] buf = new short[0];
            mWaveformView.setSamples(buf);
            mWaveformView.invalidate();
            return;
        }
        try {
            short[] buf = getAudioSample(getFile(item));
            mWaveformView.setSamples(buf);
            mWaveformView.invalidate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    File getFile(SoundItem item) {
        File root = getFilesDir();
        Log.d(DEBUG_TAG, "full item name " + item.getUri().getPath());
        File f = new File(root, item.getUri().getPath());
        return f;
    }

    private short[] getAudioSample(File f) throws IOException {
        Log.d(DEBUG_TAG, "in getAudioSample()");
        byte[] data;
        int size = (int) f.length();// - 44; // length minus header length
        data = new byte[size];
        FileInputStream fis = new FileInputStream(f);
        try {
            int read = fis.read(data, 0, size);
//                if (read < size) {
//                    int remain = size - read;
//                    while (remain > 0) {
//                        read = fis.read(tmpBuff, 0, remain);
//                        System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
//                        remain -= read;
//                    }
//                }
        } catch (IOException e) {
            throw e;
        } finally {
            fis.close();
        }

        ShortBuffer sb = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        short[] samples = new short[sb.limit()];
        sb.get(samples);
        return samples;
    }
}