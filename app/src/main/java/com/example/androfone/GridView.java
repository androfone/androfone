package com.example.androfone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;

import java.util.ArrayList;

class GridView extends View {

    private static float horizontalGridAdjust;
    private int screenWidth;
    private int width;
    private int height;
    private static int gridSize;
    private Paint gridPaint;

    private ArrayList<Float> xPos;
    private ArrayList<Float> yPos;
    private float yMax;
    float mLastXStep;
    float mLastYStep;


    public GridView(Context context) {
        super(context);
        init(context, null);
    }

    public GridView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, null);
    }

    public GridView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void init(Context context, AttributeSet attrs) {

        gridPaint = new Paint();
        gridPaint.setColor(Color.LTGRAY);

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        screenWidth = displayMetrics.widthPixels;
        int baseUnit = getResources().getInteger(R.integer.unit);
//        gridSize = (int)(baseUnit * displayMetrics.density);
        gridSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, baseUnit, displayMetrics);
        float remainer = (float)screenWidth % (float)gridSize;
        horizontalGridAdjust = remainer/2;
//        Log.e("Grid", " baseUnit: " + baseUnit);
//        Log.e("Grid", " gridSize: " + gridSize);
//        Log.e("Grid", "screenWidth/gridSize: " + (float)screenWidth / (float)gridSize);
//        Log.e("Grid", "screenWidth%gridSize modulo: " + (float)screenWidth % (float)gridSize);
//        Log.e("Grid", "horizontalGridAdjust: " + horizontalGridAdjust);

        xPos = new ArrayList<>();
        yPos = new ArrayList<>();
        mLastXStep = 0;
        mLastYStep = 0;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = getMeasuredWidth();
        height = getMeasuredHeight();
//        Log.e("Grid", "pager real pixels width : " + width + " height: " + height);
        initPoints();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (float y : yPos) {
            canvas.drawLine(0, y, screenWidth, y, gridPaint);
        }
        for (float x : xPos) {
            canvas.drawLine(x, 0, x, yMax, gridPaint);
        }

    }

    private void initPoints() {

        // collect values from X=0 -> X+=Gap... till screen height 0|||hor+gap|||W
        float horizontalPosition = horizontalGridAdjust;

        while (horizontalPosition <= width) {
            xPos.add(horizontalPosition);
            horizontalPosition += gridSize;
        }

        // collect values from Y= 0 -> Y+=Gap... till grid max
        float verticalPosition = 0;

        while (verticalPosition <= height) {
            yPos.add(verticalPosition);
            yMax = verticalPosition;
            verticalPosition += gridSize;
        }

    }

    static public float getHorizontalGridAdjust() {
        return horizontalGridAdjust;
    }

    static public int getGridSize() { return gridSize; }

}
