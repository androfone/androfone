package com.example.androfone;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.lifecycle.ViewModel;

public class ToolSet extends FrameLayout {

    private final String debugTag = "ToolSet";

    public Tool filter;
    public Tool loop;
//        public HashMap<String, View> mToolMap = new HashMap<>();

    public ToolSet(@NonNull Context context) {
        super(context);
        setVisibility(INVISIBLE);
        setInstances();
    }

    // Big ugly switch as accessor to the stored views by their menu resources ImageView names
    public Tool getToolFromMenu(String name) {
        switch (name) {
            case "play_tool_item":
                break;
            case "pique_tool_item":
                break;
            case "vol_tool_item":
                break;
            case "loop_tool_item":
                Log.e(debugTag, "getToolFromMenu() loop: " + loop);
                return loop;
            case "filter_tool_item":
                Log.e(debugTag, "getToolFromMenu() filter: " + filter);
                return filter;
            case "speed_tool_item":
                break;
            case "rev_tool_item":
                break;
            case "stretch_tool_item":
                break;
            case "seq_tool_item":
                break;
            case "metro_tool_item":
                break;
            default: return null;
        }
        return null;
    }


    // Manually set each tool slot to their tool instance and add to the frame
    // Here you create and add your custom new tool to the frame
    private void setInstances() {

        Log.e(debugTag, "setInstances()");
        if (loop == null) {
            loop = new SelectionControlView(getContext());
            loop.setBackgroundColor(getResources().getColor(R.color.colorgray));
            addView(loop);
        }
        if (filter == null) {
            filter = new FilterControlView(getContext());
//            filter.setBackgroundColor(getResources().getColor(R.color.colorgray));
            addView(filter);
        }
    }

    // Static method to get new instance of a tool. Call the convenience constructor of those tools
    // allowing the set up of the tool with the viewmodel in one go.
    public static Tool getInstanceByClassName(Context context, WorkspaceViewModel viewModel, String classname) {
        switch (classname) {
            case "FilterControlView":
                return new FilterControlView(context, viewModel);
            case "SelectionControlView":
                return new SelectionControlView(context, viewModel);
        }
        return null;
    }
}