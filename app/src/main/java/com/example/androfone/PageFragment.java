package com.example.androfone;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;

// Instances of this class are fragments representing a single
// page in the workspace.
public class PageFragment extends Fragment {
    public static final String ARG_OBJECT = "object";

    private static final String CHILD_LIST_KEY = "CHILD_LIST_KEY";
    private static final String CHILD_CLASS_KEY = "CHILD_CLASS_KEY";
    private static final String CHILD_POS_KEY = "CHILD_POS_KEY";

    private LocalState mLocalState = null;

    private RelativeLayout mRoot;
    public GridView mGrid;
    public RelativeLayout mCollisionLayer;
    public RelativeLayout mToolLayer;
    private String debugTag;
    private MainViewModel mMainViewModel;
    private WorkspaceViewModel mViewModel;


    public static PageFragment newInstance(int value) {

        PageFragment fragment =
                new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_OBJECT, value);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRoot = (RelativeLayout)inflater.inflate(R.layout.page_fragment, container, false);
        TextView textView = mRoot.findViewById(R.id.text1);
        mGrid = mRoot.findViewById(R.id.grid_layer);
        mCollisionLayer = mRoot.findViewById(R.id.collision_layer);
        mToolLayer = mRoot.findViewById(R.id.tool_layer);

        if (getArguments() != null) {
            textView.setText(Integer.toString(getArguments().getInt(ARG_OBJECT)+1));
        }
        if (savedInstanceState != null) {

        }
        if (mLocalState != null) {
            restoreFromLocalState();
        }

        return mRoot;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        debugTag = "PageFrag_" + this.getTag();

        mViewModel = new ViewModelProvider(this.getParentFragment()).get(WorkspaceViewModel.class);
        mViewModel.getEditMode().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    mGrid.setVisibility(View.VISIBLE);
                } else {
                    mGrid.setVisibility(View.GONE);
                }
            }
        });
    }

    public RelativeLayout getToolLayer() { return mToolLayer; }

    public RelativeLayout getCollisionLayer() { return mCollisionLayer; }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

        if (mToolLayer.getChildCount() > 0) {
            for (int i=0; i<mToolLayer.getChildCount(); i++) {
                View child = mToolLayer.getChildAt(i);
                String className = child.getClass().getSimpleName();

                float[] pos = new float[2];
                pos[0] = child.getX();
                pos[1] = child.getY();
                Bundle viewBundle = new Bundle();
                viewBundle.putString(CHILD_CLASS_KEY, className);
                viewBundle.putFloatArray(CHILD_POS_KEY, pos);

                Log.e(debugTag, "********* CustomView id: " + mToolLayer.getChildAt(0).getId());
                Log.e(debugTag, "********* viewBundle: " + viewBundle);
                outState.putBundle(CHILD_LIST_KEY, viewBundle);
            }
        }

        Log.e(debugTag, "onSaveInstanceState savedState: " + outState);
        super.onSaveInstanceState(outState);
        Log.e(debugTag, "onSaveInstanceState.root.getChildCount(): " + mToolLayer.getChildCount());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Parcelable newState = savedInstanceState;
        Log.e(debugTag, "onViewStateRestored state: " + savedInstanceState);
        Log.e(debugTag, "this: " + this);

        if (savedInstanceState instanceof Bundle) {

            if (savedInstanceState.getBundle(CHILD_LIST_KEY) != null) {
                Log.e(debugTag, "Reading children state from bundle");
                Bundle child = savedInstanceState.getBundle(CHILD_LIST_KEY);
                float[] pos = child.getFloatArray(CHILD_POS_KEY);
                String className = child.getString(CHILD_CLASS_KEY);

//                if (className.equals("CustomView")) {
//                    CustomView v =  new CustomView(getActivity());
//                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(300, 200);
//                    v.setLayoutParams(lp);
//                    v.setX(pos[0]);
//                    v.setY(pos[1]);
//                    root.addView(v);
//                    root.invalidate();
//                    Log.i(debugTag, "retreived view: " + v + " id: " + v.getId());
//                }

            }
            newState = (savedInstanceState).getParcelable("SUPER_STATE_KEY");
        }
        super.onViewStateRestored((Bundle)newState);
        Log.e(debugTag, "onViewStateRestored.mToolLayer.getChildCount(): " + mToolLayer.getChildCount());
    }

    public void saveLocalState() {

        if (this != null && this.getTag() != null) {
            Log.e(debugTag, "!!!!!!! saveLocalState() " + this + " !!!!!!!!");
            mLocalState = new LocalState();

            if (mToolLayer.getChildCount() > 0) {
                for (int i=0; i<mToolLayer.getChildCount(); i++) {
                    View child = mToolLayer.getChildAt(i);
                    LocalState.Child localchild = mLocalState.getChild();
                    localchild.className = child.getClass().getSimpleName();
                    float[] pos = new float[2];
                    pos[0] = child.getX();
                    pos[1] = child.getY();
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) child.getLayoutParams();
                    Log.e(debugTag, "********* LocalState.lp w h: " + lp.width +" "+lp.height);
                    int left = lp.leftMargin;
                    int top = lp.topMargin;
                    localchild.position = pos;
                    Log.e(debugTag, "********* LocalState.position x y: " + pos[0] +" "+ pos[1]);
                    Log.e(debugTag, "********* LocalState.margin x y: " + left +" "+ top);
                    mLocalState.childList.add(localchild); // bug here: child will add up - reset the list
                    Log.e(debugTag, "********* LocalState.ChildList.size: " + mLocalState.childList.size());
                    Log.e(debugTag, "********* LocalState.Child: " + localchild);
                }
            }
        }
    }

    private void restoreFromLocalState() {
        Log.e(debugTag, "!!!!!!! restoreFromLocalState() !!!!!!!!");
        if (!mLocalState.childList.isEmpty()) {
            for (LocalState.Child child : mLocalState.childList) {
                Log.i(debugTag, "Reading children state from localState");
                float[] pos = child.position;
                String className = child.className;
                View tool = ToolSet.getInstanceByClassName(getParentFragment().getContext(), mViewModel, className);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-2, -2);
                lp.leftMargin = (int)pos[0];
                lp.topMargin = (int)pos[1];
                tool.setLayoutParams(lp);
                Log.i(debugTag, "retrieved viewpos: " + pos[0] + "  " + pos[1]);
                Log.i(debugTag, "retrieved viewlayoutpaarams: " + lp.leftMargin +" "+ lp.topMargin);
                mToolLayer.addView(tool); // Add recreated new tool View;
                mToolLayer.invalidate();
                Log.i(debugTag, "retrieved view: " + tool + " id: " + tool.getId());
            }
        }
    }

    private class LocalState {
        ArrayList<Child> childList = new ArrayList<>();
        Child getChild() {
            return new Child();
        }
        private class Child {
            String className;
            float[] position = new float[2];
        }
    }
}
