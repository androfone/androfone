package com.example.androfone;

import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.customview.widget.Openable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.HashMap;

public class SlidingMenuLayout extends LinearLayoutCompat implements Openable, View.OnTouchListener {


    public interface OnToolSelectedListener {
        public void onToolSelected(View v);
    }

    private int mClosedWidth;
    private int mOpenedWidth;
    private boolean isOpen = false;
    public View mHeader;
    private final String debugTag = "SlidingMenuLayout";
    private boolean isInited = false;
    public ToolSet mTools;
    public HashMap<String, Tool> mToolMap = new HashMap<>();
    public HashMap<View, Integer> mToolIconMap = new HashMap<>();
    private OnToolSelectedListener mToolListener;


    public SlidingMenuLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SlidingMenuLayout, 0, 0);
        isOpen = a.getBoolean(R.styleable.SlidingMenuLayout_is_open, false);
        a.recycle();

        mClosedWidth = -1;
        mOpenedWidth = -1;

        inflate(context, R.layout.tool_menu_layout, this);

        mHeader = findViewById(R.id.tool_header);
        LinearLayout items = findViewById(R.id.item_layout);
        for (int i=0; i<items.getChildCount(); i++) {
            Log.e(debugTag, "child count: " + i);
           items.getChildAt(i).setOnTouchListener(this);
//           mToolMap.put(items.getChildAt(i).getResources().getResourceEntryName(items.getChildAt(i).getId()), null);
//           mToolIconMap.put(items.getChildAt(i), i);
        }

    }

    public void setOnToolSelectedListener(OnToolSelectedListener listener) {
        mToolListener = listener;
        Log.e(debugTag, "setOnToolSelectedListener");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        Log.e(debugTag, "onMeasure(): presuper widthMeasureSpec size: " + MeasureSpec.getSize(widthMeasureSpec));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int mode = MeasureSpec.getMode(widthMeasureSpec);
        int size = MeasureSpec.getSize(widthMeasureSpec);
//        int size = 600;
//        Log.e(debugTag, "onMeasure(): MeasureSpec.getSize(widthMeasureSpec): " + size);
        if (mOpenedWidth == -1) mOpenedWidth = size;
        int minsize = getChildAt(0).getMeasuredWidth();
        if (mClosedWidth == -1) {
            mClosedWidth = minsize;
        }
        Log.e(debugTag, "onMeasure(): /*/*/*/**/*/*/**/*/*/**/*/ mOpenedWidth: " + mOpenedWidth);

        setMeasuredDimension(isOpen ? mOpenedWidth : mClosedWidth, heightMeasureSpec);
//        Log.e(debugTag, "onMeasure(): widthMeasureSpec minsize: " + mClosedWidth);
//        Log.e(debugTag, "onMeasure(): widthMeasureSpec mode, size: " + mode +" "+ size);
    }


    public void setOpened(boolean b) {
        if (b) open();
        else close();
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void open() {
        ValueAnimator anim = ValueAnimator.ofInt(mClosedWidth, mOpenedWidth);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Log.e(debugTag, "open(): valueAnimator: " + (Integer) valueAnimator.getAnimatedValue());
                getLayoutParams().width = (Integer) valueAnimator.getAnimatedValue();
                requestLayout();
            }

        });
        if (isInited) anim.setDuration(333);
        else anim.setDuration(0);
        anim.start();

        isOpen = true;
        setActivated(true);
        mHeader.setActivated(true);
        mTools = new ToolSet(getContext());
        addView(mTools);

//        requestLayout();
//        getLayoutParams().width = (Integer) valueAnimator.getAnimatedValue();+ " isOpen: " + isOpen + " mOpenedWidth: " + mOpenedWidth);
    }


    @Override
    public void close() {
        ValueAnimator anim = ValueAnimator.ofInt(mOpenedWidth, mClosedWidth);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                getLayoutParams().width = (Integer) valueAnimator.getAnimatedValue();
                requestLayout();
            }
        });
        if (isInited) anim.setDuration(111);
        else anim.setDuration(0);
        anim.start();

        isOpen = false;
        setActivated(false);
        mHeader.setActivated(false);
        requestLayout();
        removeView(mTools);
        mTools = null;
//        Log.e(debugTag, "close(): width: " + this.getWidth() + " isOpen: " + isOpen + " mOpenedWidth: " + mOpenedWidth);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.e(debugTag, "items_layout onTouch view: " + v.getResources().getResourceEntryName(v.getId()));
        Log.e(debugTag, "items_layout onTouch view: " + v);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Log.e(debugTag, "item onTouch action_down.");
            Tool tool = mTools.getToolFromMenu(v.getResources().getResourceEntryName(v.getId()));
            Log.e(debugTag, "mToolListener: " + mToolListener);
            if (mToolListener != null) { mToolListener.onToolSelected(tool); }


//
//            ClipData data = ClipData.newPlainText("MENU", "");
//
////            f.setClickPointOffset(filter.getWidth()/2, filter.getHeight()/2);
//            View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(tool);
////            f.collectRectangles();
//            tool.startDrag(data, dragShadowBuilder, tool, 0);
//            tool.setVisibility(View.INVISIBLE);
//            getRootView().invalidate();
            return true;
        } else {
            return false;
        }
    }
}
