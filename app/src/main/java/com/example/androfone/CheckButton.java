package com.example.androfone;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;

public class CheckButton extends androidx.appcompat.widget.AppCompatButton implements Checkable {

    private static final int[] CheckedStateSet = { android.R.attr.state_checked };
    boolean mChecked = false;

    public interface OnStateChangedListener {
        void onStateChanged(boolean state);
    }
    private OnStateChangedListener mListener;

    public CheckButton(Context context) {
        super(context);
        setOnClickListener(v -> toggle());
        setSoundEffectsEnabled(false);
    }

    public CheckButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(v -> toggle());
        setSoundEffectsEnabled(false);
    }

    public void setOnStateChangedListener(OnStateChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        mChecked = !mChecked;
        if (mListener != null) {
            mListener.onStateChanged(mChecked);
        }
        refreshDrawableState();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CheckedStateSet);
        }
        return drawableState;
    }
}
