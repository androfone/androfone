package com.example.androfone.filedialog;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androfone.R;
import com.example.androfone.filedialog.utils.PlaybackListener;
import com.example.androfone.filedialog.utils.PreviewPlayer;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserFragment extends Fragment implements TextInputDialogFragment.OnOKButtonClickListener {

    private static final String DEBUG_TAG = "UserFragment";

    private static final String EDIT_STATE_KEY = "editState";


    private FileDialogAdapter mAdapter;
    private RecyclerView mRecycler;
    private ItemTouchHelper mHelper;
    private boolean isEditMode = false;
    private SoundViewModel soundViewModel;
    FileDialogAdapter.SoundHolder lastHolder;
    SoundItem lastItemPlaying;
    private PreviewPlayer mPlayer;
    RecorderFragment recordFrag;

    private FileFragment.OnItemSelectedListener mItemSelectedListener;

    public void setOnItemSelectedListener(FileFragment.OnItemSelectedListener listener) {
        this.mItemSelectedListener = listener;
    }

    public UserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fd_fragment_user, container, false);
        mRecycler = root.findViewById(R.id.record_list);

        // This is where we start the RecorderFragment and keep it alive through setRetainInstance
        FragmentContainerView fragFrame = root.findViewById(R.id.record_frame);
        String tag = "record";
        recordFrag = (RecorderFragment) getChildFragmentManager().findFragmentByTag(tag);
        if (recordFrag == null) {
            recordFrag = new RecorderFragment();
            recordFrag.setRetainInstance(true);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(fragFrame.getId(), recordFrag, tag).commit();
            transaction.setMaxLifecycle(recordFrag, Lifecycle.State.RESUMED);
        }

        recordFrag.setOnRecordFinishedListener(file -> showInputDialog(null, -1));
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        soundViewModel = new ViewModelProvider(getParentFragment()).get(SoundViewModel.class);

        mAdapter = new FileDialogAdapter(FileDialogAdapter.USER_ADAPTER, (item, position) -> {
            if (isEditMode) { // item selected to be edited - get input dialog
                Log.d(DEBUG_TAG, "onItemSelected(SoundItem item) editMode item: " + item.getName());
                showInputDialog(item, position);
            } else { // item is selected - get back to workspace
                Log.d(DEBUG_TAG, "onItemSelected(SoundItem item) item: " + item.getName());
                if (mItemSelectedListener != null) {
                    mItemSelectedListener.onItemSelected(item, position);
                }
            }
        });
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);
        mRecycler.setHasFixedSize(true);

        mPlayer = new PreviewPlayer(new PlaybackListener() {
            @Override
            public void onProgress(int progress) {
            }

            @Override
            public void onCompletion() {
                mPlayer.stopPlayer();
                if (lastHolder != null) lastHolder.setPlaying(false);
            }
        });

        mAdapter.setOnPlayPreviewListener((holder) -> {
            SoundItem item = mAdapter.getItemAt(holder.getAdapterPosition());
            boolean isPlaying = !holder.isPlaying();
            if (isPlaying && mPlayer.isPlayerRunning()) {
                Log.d(DEBUG_TAG, "onPlayPreview - player already running name: " + lastItemPlaying.getName());
                mPlayer.stopPlayer();
                lastHolder.setPlaying(false);
            }
            if (isPlaying) {
                mPlayer.startPlayer(item);
                holder.setPlaying(true);
                lastHolder = holder;
                lastItemPlaying = item;
            } else {
                mPlayer.stopPlayer();
                holder.setPlaying(false);
            }
        });

        mAdapter.setOnFavoriteChangedListener(item -> {
            Log.d(DEBUG_TAG, "userfavoriteChanged");
            soundViewModel.update(item);
        });
        mHelper = new ItemTouchHelper(new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                soundViewModel.delete(mAdapter.getItemAt(viewHolder.getAdapterPosition()));

            }
        }); // Do not attach to recyclerView yet - attach depend on editMode, see below

        // set up viewmodel observer for editmode
        soundViewModel.getEditMode().observe(getViewLifecycleOwner(), aBoolean -> {
            isEditMode = aBoolean;
            Log.d(DEBUG_TAG, "soundViewModel.getEditMode(): " + isEditMode);
            if (isEditMode) {
                mHelper.attachToRecyclerView(mRecycler);
                mAdapter.isEditMode = isEditMode;
                mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount(), null);
            } else {
                mAdapter.isEditMode = false;
                mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount(), null);
                mHelper.attachToRecyclerView(null);
            }
        });

        soundViewModel.getLiveUserSoundsList().observe(getViewLifecycleOwner(), soundItems -> {
            Log.d(DEBUG_TAG, "observing userSounds list");
            soundViewModel.setEditEnable(soundItems.size() > 0);
            mAdapter.submitList(soundItems);
        });

        // enable disable edit button depending on list empty or not
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                Log.d(DEBUG_TAG, "RecyclerView.AdapterDataObserver changed: " + mAdapter.getItemCount());
                super.onChanged();
                if (mAdapter.getItemCount() != 0) { // If there is some items
                        soundViewModel.setEditEnable(true); // set editEnable true
                }
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                Log.d(DEBUG_TAG, "RecyclerView.AdapterDataObserver removed: " + mAdapter.getItemCount());
                if (mAdapter.getItemCount() == 0) {
                    soundViewModel.setEditEnable(false);
                }
            }
        });
        // also need this to test if list is empty on create
        soundViewModel.setEditEnable(mAdapter.getItemCount() != 0);

        if (savedInstanceState != null) {
            Log.d(DEBUG_TAG, "onViewCreated  savedInstanceState != null isEditmode " + isEditMode);
            boolean b = savedInstanceState.getBoolean(EDIT_STATE_KEY);
            soundViewModel.setEditMode(b);
            Log.d(DEBUG_TAG, "onViewCreated  savedInstanceState != null isEditmode " + b);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(EDIT_STATE_KEY, isEditMode);
        Log.d(DEBUG_TAG, "onSaveInstanceState(@NonNull Bundle outState) isEditmode " + isEditMode);
        super.onSaveInstanceState(outState);
    }

    // TextInputDialog for name or rename item used by both record button.onStop and holder.name->onItemClick
    private void showInputDialog(@Nullable final SoundItem item, int position) {

        final boolean isSaveDialog = position == -1;
        final String inputName = !isSaveDialog ? item.getName() : "";

        if (!isSaveDialog) {
            soundViewModel.setDialogParameters(position);
        }
        final TextInputDialogFragment dialog = TextInputDialogFragment.newInstance(inputName);

        dialog.show(getChildFragmentManager(), "text_input_dialog");
    }

    @Override
    public void onOKButtonClick(TextInputDialogFragment dialog) {
        String result = dialog.getInputView().getText().toString();
        Log.d(DEBUG_TAG, "onOKButtonClick(TextInputDialogFragment dialog)");
        Log.d(DEBUG_TAG, "onOKButtonClick result:" + result);
        // create a new file with this name
        // but check first if a file with that name already exist
        boolean exist = soundViewModel.isUserSoundItemExist(result);
        if (exist) {
            Log.d(DEBUG_TAG, "UserSoundItemExist!");
            Toast toast = Toast.makeText(getActivity(),
                    "Name already exist! Try again", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                    0, 0);
            toast.show();
        } else {
            // OK to save or rename
            if (dialog.isSaveDialog()) { // This should be a new recorded file
                Log.d(DEBUG_TAG, "saveButton onClick save result :" + result);
                soundViewModel.createNewUserSoundItem(result);
            } else { // change the name of existing file
                int pos = soundViewModel.getPositionDialogParameter();
                soundViewModel.renameUserSoundItem(result, mAdapter.getItemAt(pos));
                Log.d(DEBUG_TAG, "item name before closing dialog: " + mAdapter.getItemAt(pos).getName());
                mAdapter.notifyItemChanged(pos);
                soundViewModel.resetDialogParameters();
            }
            dialog.dismiss();
        }
    }

    // Fragment stop, make sure to stop player
    @Override
    public void onStop() {
        super.onStop();
        mPlayer.stopPlayer();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach()");
//        soundViewModel.setEditMode(false);
    }
    @Override
    public void onStart() {
        Log.d(DEBUG_TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(DEBUG_TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(DEBUG_TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        Log.d(DEBUG_TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        Log.d(DEBUG_TAG, "onConfigurationChanged(@NonNull Configuration newConfig)");
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        Log.d(DEBUG_TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(DEBUG_TAG, "onAttach(@NonNull Context context)");
        super.onAttach(context);
    }
}
