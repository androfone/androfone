package com.example.androfone.filedialog;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.io.File;
import java.util.List;

public class SoundViewModel extends AndroidViewModel {

    private static final String DEBUG_TAG = "SoundViewModel";

    private final SoundRepository repository;
    private final LiveData<List<SoundItem>> internalSoundsList;
    private final LiveData<List<SoundItem>> soundByDirectory;
    private final LiveData<List<SoundItem>> userSoundsList;
    private final LiveData<List<SoundItem>> favoritesList;
    private final MutableLiveData<Long> directoryId = new MutableLiveData<>();
    private final LiveData<List<DirectoryItem>> directoryList;
//    private final LiveData<DirectoryItem> userDirectory;
//    private final DirectoryItem userDirectory;

    // Record control
    private final MutableLiveData<Boolean> isRecording = new MutableLiveData<>();

    public SoundViewModel(@NonNull Application application) {
        super(application);

        Log.d(DEBUG_TAG, "SoundViewModel begin");
        repository = new SoundRepository(application);
        Log.d(DEBUG_TAG, "SoundViewModel returning from new SoundRepository");

        directoryList = repository.getDirectoryList();
//        userDirectory = repository.getUserDirectory();

        soundByDirectory = Transformations.switchMap(directoryId,
                dir -> repository.getSoundsByDirectory(dir));
        internalSoundsList = repository.getInternalSoundList();
        userSoundsList = repository.getUserSoundList();
        favoritesList = repository.getFavorites();

    }

    // Record control
    public void setIsRecording(boolean active) {
        isRecording.postValue(active);
    }

    public LiveData<Boolean> getIsRecording() {
        return isRecording;
    }

    private final MutableLiveData<Float> recordProgress = new MutableLiveData<>();

    public void setRecordProgress(Float progress) {
        recordProgress.postValue(progress);
    }

    public LiveData<Float> getRecordProgress() {
        return recordProgress;
    }


    // FileDialog editEnable and editMode data - not the same as Workspace editMode
    private final MutableLiveData<Boolean> editEnable = new MutableLiveData<>();

    public void setEditEnable(boolean active) {
        editEnable.setValue(active);
    }

    public LiveData<Boolean> getEditEnable() {
        return editEnable;
    }

    private final MutableLiveData<Boolean> editMode = new MutableLiveData<>();

    public void setEditMode(boolean active) {
        editMode.setValue(active);
    }

    public LiveData<Boolean> getEditMode() {
        return editMode;
    }

    // Repository and Database access method and data
    public void insert(SoundItem sound) {
        repository.insert(sound);
    }

    public void update(SoundItem sound) {
        repository.update(sound);
    }

    public void delete(SoundItem sound) {
        repository.delete(sound);
    }

    public LiveData<List<SoundItem>> getLiveInternalSoundsList() {
        return internalSoundsList;
    }

    public LiveData<List<SoundItem>> getLiveSoundByDirectoryList() {
        return soundByDirectory;
    }

    public void setDirectoryId(Long id) {
        directoryId.setValue(id);
    }

    public LiveData<List<SoundItem>> getLiveUserSoundsList() {
        return userSoundsList;
    }

    public LiveData<List<SoundItem>> getLiveFavoritesList() {
        return favoritesList;
    }

    public LiveData<List<DirectoryItem>> getLiveDirectoryList() {
        return directoryList;
    }

//    public LiveData<DirectoryItem> getUserDirectory() { return userDirectory; }
//    public DirectoryItem getUserDirectory() {
//        return userDirectory;
//    }

    public boolean isUserSoundItemExist(String name) {
        Log.d(DEBUG_TAG, "isUserSoundItemExist(String name) " + name);
        List<SoundItem> userList = userSoundsList.getValue();
        if (userList != null) {
            for (SoundItem item : userList) {
                if (item != null) {
                    if (name.equals(item.getName())) return true; // early return if same name
                }
            }
        }
        return false;
    }

    public void renameUserSoundItem(String newName, SoundItem item) {
        repository.renameSoundFile(newName, item);
    }

    public void createNewUserSoundItem(String name) {
        repository.createNewSoundItem(name, transientRecordingFile);
//        transientRecordingFile = null;
    }

    ////// Transient recording file dialog parameters
    public File transientRecordingFile;
    private int position = -1;


    // User by UserFragment to store input dialog parameters
    // Could perhaps use saveInstanceState parameter instead
    public void setDialogParameters(int position) {
        this.position = position;
    }

    public int getPositionDialogParameter() {
        return position;
    }

    public void resetDialogParameters() {
        this.position = -1;
    }


}
