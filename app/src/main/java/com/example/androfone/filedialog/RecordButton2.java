package com.example.androfone.filedialog;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.androfone.R;

/*
*  RecordButton2 is really just a shell that will react to external parameters.
*  It set the inner shape to a circle or a square (start/stop) trough setActivated(boolean active)
*  and the progress indicator is set trough setIndicator(float progress)
*/

public class RecordButton2 extends View {

    private static final String DEBUG_TAG = "RecordButton";


    private Paint mRingPaint, mFillPaint, mCenterPaint, mIndicatorPaint;

    private int mWidth, mHeight;

    private Path mButtonPath;
    private Path mRingPath;
    private Path mRecordPath;
    private Path mStopPath;
    private Path mInnerPath;

    private float mArcAngle;
    private RectF mIndicatorRect;

    public RecordButton2(Context context) {
        super(context);
        init(context, null, 0);
    }

    public RecordButton2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RecordButton2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        int ringColor = ContextCompat.getColor(context, R.color.colorAccent);
        int fillColor = ContextCompat.getColor(context, R.color.colorPrimaryExtraLightLighter);
        int centerColor = ContextCompat.getColor(context, android.R.color.holo_red_dark);

        mRingPaint = new Paint();
        mRingPaint.setColor(ringColor);
        mRingPaint.setStyle(Paint.Style.FILL);
        // 2 on Samsung GalaxyTab
        mRingPaint.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.thin_size));
        mRingPaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(fillColor);

        mCenterPaint = new Paint();
        mCenterPaint.setStyle(Paint.Style.FILL);
        mCenterPaint.setAntiAlias(true);
        mCenterPaint.setColor(centerColor);

        mIndicatorPaint = new Paint();
        mIndicatorPaint.setColor(centerColor);
        mIndicatorPaint.setStyle(Paint.Style.STROKE);
        mIndicatorPaint.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.medium_size));
//        mIndicatorPaint.setStrokeWidth(8); // 8 on Samsung GalaxyTab
        mIndicatorPaint.setAntiAlias(true);
    }

    public void setIndicator(float progress) {
        mArcAngle = progress * 360f;
    }

    @Override
    public void setActivated(boolean activated) {
        super.setActivated(activated);
        if (isActivated()) {
            mInnerPath = mStopPath;
        } else {
            mInnerPath = mRecordPath;
            mArcAngle = 0;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();

        createView();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        Log.d(DEBUG_TAG, "angle " + mArcAngle );

        canvas.drawPath(mRingPath, mRingPaint);
        canvas.drawPath(mButtonPath, mFillPaint);
        canvas.drawPath(mInnerPath, mCenterPaint);
        canvas.drawArc(mIndicatorRect,
                -90f, mArcAngle, false, mIndicatorPaint);
    }

    private void createView() {
        if (mWidth <= 0 || mHeight <= 0)
            return;


        float centerX = mWidth / 2f;
        float centerY = mHeight / 2f;
        float radius = (mWidth / 2f);
        float innerRadius = radius / 2.25f;
        float mIndicatorRadius = radius * 0.92f;
        mIndicatorRect = new RectF(centerX - mIndicatorRadius, centerY - mIndicatorRadius,
                centerX + mIndicatorRadius, centerY + mIndicatorRadius);

        mButtonPath = new Path();
        mButtonPath.addCircle(centerX, centerY, radius * 0.96f, Path.Direction.CCW);

        mRingPath = new Path();
        mRingPath.addCircle(centerX, centerY, radius, Path.Direction.CCW);
        RectF r = new RectF(0, 0, mWidth, mHeight);

        // setup inner shapes: recordPath = circle, stopPath = square
        mRecordPath = new Path();
        mRecordPath.addCircle(centerX, centerY, innerRadius, Path.Direction.CCW);
        mStopPath = new Path();
        r.set(centerX - innerRadius, centerY - innerRadius,
                centerX + innerRadius, centerY + innerRadius);
        mStopPath.addRect(r, Path.Direction.CCW);

        invalidate();

        // initialize innerpath to record
        mInnerPath = mRecordPath;
    }
}
