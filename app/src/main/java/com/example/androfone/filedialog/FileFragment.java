package com.example.androfone.filedialog;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androfone.R;
import com.example.androfone.filedialog.utils.PlaybackListener;
import com.example.androfone.filedialog.utils.PreviewPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileFragment extends Fragment {

    private static final String DEBUG_TAG = "FileFragment";

    private SoundViewModel soundViewModel;
    private FileDialogAdapter mAdapter;
    private RecyclerView mRecycler;

    private SoundItem lastItemPlaying = null;
    private FileDialogAdapter.SoundHolder lastHolder;
    private PreviewPlayer mPlayer;

    public interface OnItemSelectedListener {
        void onItemSelected(SoundItem item, int position);
    }

    private OnItemSelectedListener mItemSelectedListener;

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        this.mItemSelectedListener = listener;
    }


    public FileFragment() {
        // Required empty public constructor
    }

    public static FileFragment getInstance() {
        return new FileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fd_fragment_file, container, false);
        mRecycler = root.findViewById(R.id.file_list);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mAdapter = new FileDialogAdapter(FileDialogAdapter.FILE_ADAPTER, (item, position) -> {
            Log.d(DEBUG_TAG, "onItemSelected(SoundItem item) called item: " + item.getName());
            if (mItemSelectedListener != null) {
                Log.d(DEBUG_TAG, "mItemSelectedListener != null");
                mItemSelectedListener.onItemSelected(item, position);
            }
        });

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);
        mRecycler.setHasFixedSize(true);

        soundViewModel = new ViewModelProvider(getParentFragment()).get(SoundViewModel.class);

        soundViewModel.getLiveSoundByDirectoryList().observe(getViewLifecycleOwner(), soundItems -> {
            Log.d(DEBUG_TAG, "getLiveSoundByDirectoryList() onChanged() called");
            mAdapter.submitList(soundItems);
        });

        mAdapter.setOnFavoriteChangedListener(item -> {
            Log.d(DEBUG_TAG, "soundListfavoriteChanged");
            soundViewModel.update(item);
        });

        mPlayer = new PreviewPlayer(new PlaybackListener() {
            @Override
            public void onProgress(int progress) { }

            @Override
            public void onCompletion() {
                mPlayer.stopPlayer();
                if (lastHolder != null) lastHolder.setPlaying(false);
            }
        });

        mAdapter.setOnPlayPreviewListener((holder) -> {
            SoundItem item = mAdapter.getItemAt(holder.getAdapterPosition());
            boolean isPlaying = !holder.isPlaying();
            if (isPlaying && mPlayer.isPlayerRunning()) {
                Log.d(DEBUG_TAG, "onPlayPreview - player already running name: " + lastItemPlaying.getName());
                mPlayer.stopPlayer();
                lastHolder.setPlaying(false);
            }
            if (isPlaying) {
                mPlayer.startPlayer(item);
                holder.setPlaying(true);
                lastHolder = holder;
                lastItemPlaying = item;
            } else {
                mPlayer.stopPlayer();
                holder.setPlaying(false);
            }
        });
    }

    // Fragment stop, make sure to stop player
    @Override
    public void onStop() {
        Log.d(DEBUG_TAG, "onStop()");
        super.onStop();
        mPlayer.stopPlayer();
//        mPlayer = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onViewStateRestored(@Nullable Bundle savedInstanceState)");
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.d(DEBUG_TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(DEBUG_TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(DEBUG_TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onDetach() {
        Log.d(DEBUG_TAG, "onDetach()");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(DEBUG_TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        Log.d(DEBUG_TAG, "onConfigurationChanged(@NonNull Configuration newConfig)");
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        Log.d(DEBUG_TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(DEBUG_TAG, "onAttach(@NonNull Context context)");
        super.onAttach(context);
    }
}
