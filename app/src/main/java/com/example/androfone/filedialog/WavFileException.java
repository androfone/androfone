package com.example.androfone.filedialog;

/**
 * @author Anna Stępień <anna.stepien@semantive.com>
 */
/**
 * This file is a copy from https://github.com/Semantive/waveform-android
 * in dir:
 * waveform-android/library/src/main/java/com/semantive/waveformandroid/waveform/soundfile/
 */
public class WavFileException extends Exception {

    public WavFileException(final String message) {
        super(message);
    }
}