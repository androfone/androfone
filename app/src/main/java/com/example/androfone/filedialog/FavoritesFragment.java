package com.example.androfone.filedialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androfone.R;
import com.example.androfone.filedialog.utils.PlaybackListener;
import com.example.androfone.filedialog.utils.PlaybackThread;
import com.example.androfone.filedialog.utils.PreviewPlayer;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {

    private static final String DEBUG_TAG = "FavoriteFragment";

    private static final String EDIT_STATE_KEY = "editState";

    private FileDialogAdapter mAdapter;
    private RecyclerView mRecycler;
    private TextView mMessage;
    private ItemTouchHelper mHelper;
    private boolean isEditMode = false;
    private SoundItem lastItemPlaying = null;
    boolean isPlayerRunning = false;
    PlaybackThread mPlaybackThread;
    PlaybackListener mPlaybackListener;
    FileDialogAdapter.SoundHolder lastHolder;
    private PreviewPlayer mPlayer;

    private SoundViewModel soundViewModel;

    private FileFragment.OnItemSelectedListener mItemSelectedListener;
    public void setOnItemSelectedListener(FileFragment.OnItemSelectedListener listener) {
        this.mItemSelectedListener = listener;
    }

    public FavoritesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fd_fragment_favorite, container, false);
        mRecycler = root.findViewById(R.id.favorite_list);
        mMessage = root.findViewById(R.id.message_view);
//        soundViewModel = new ViewModelProvider(requireActivity()).get(SoundViewModel.class);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        soundViewModel = new ViewModelProvider(getParentFragment()).get(SoundViewModel.class);
        mAdapter = new FileDialogAdapter(FileDialogAdapter.FAVORITES_ADAPTER, (item, position) -> {
            Log.d(DEBUG_TAG, "onItemSelected(SoundItem item) called item: " + item.getName());
            if (mItemSelectedListener != null) {
                mItemSelectedListener.onItemSelected(item, position);
            }
        });

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setHasFixedSize(true);
        mRecycler.setAdapter(mAdapter);

        mHelper = new ItemTouchHelper(new ItemTouchHelper
                .SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                SoundItem item = mAdapter.getItemAt(viewHolder.getAdapterPosition());
                Log.d(DEBUG_TAG, "item Swiped item: " + item + " name: " + item.getName());
                item.setFavorite(false);
                soundViewModel.update(item);
            }

        });

        // get the favorites list
        soundViewModel.getLiveFavoritesList().observe(getViewLifecycleOwner(), soundItems -> {
            Log.d(DEBUG_TAG, "observing FavoritesSounds list");
            Log.d(DEBUG_TAG, "observing FavoritesSounds changed: listsize " + soundItems.size());
            soundViewModel.setEditEnable(soundItems.size() > 0);
            mMessage.setVisibility(soundItems.size() > 0 ? View.GONE : View.VISIBLE);
            mAdapter.submitList(soundItems);
        });

        // set up modelview observer for editmode
        soundViewModel.getEditMode().observe(getViewLifecycleOwner(), aBoolean -> {
            isEditMode = aBoolean;
            if (isEditMode) {
                mHelper.attachToRecyclerView(mRecycler);
                mAdapter.isEditMode = isEditMode;
                mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount(), null);

            } else {
                mAdapter.isEditMode = isEditMode;
                mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount(), null);
                mHelper.attachToRecyclerView(null);
            }
        });

        // if list get empty, disable edit mode and display message
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                Log.d(DEBUG_TAG, "RecyclerView.AdapterDataObserver changed: " + mAdapter.getItemCount());
                super.onChanged();
                if (mAdapter.getItemCount() != 0) {
//                    if (!isEditMode) {
                        soundViewModel.setEditEnable(true);
//                    }
                    if (mMessage.getVisibility() == View.VISIBLE) { // remove message if visible
                        mMessage.setVisibility(View.GONE);

                    }
                }
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                Log.d(DEBUG_TAG, "RecyclerView.AdapterDataObserver removed: " + mAdapter.getItemCount());
                if (mAdapter.getItemCount() == 0) {
                    soundViewModel.setEditEnable(false);
                    mMessage.setVisibility(View.VISIBLE); // display message
                }
            }
        });

        mPlayer = new PreviewPlayer(new PlaybackListener() {
            @Override
            public void onProgress(int progress) { }

            @Override
            public void onCompletion() {
                mPlayer.stopPlayer();
                if (lastHolder != null) lastHolder.setPlaying(false);
            }
        });

        mAdapter.setOnPlayPreviewListener((holder) -> {
            SoundItem item = mAdapter.getItemAt(holder.getAdapterPosition());
            boolean isPlaying = !holder.isPlaying();
            if (isPlaying && mPlayer.isPlayerRunning()) {
                Log.d(DEBUG_TAG, "onPlayPreview - player already running name: " + lastItemPlaying.getName());
                mPlayer.stopPlayer();
                lastHolder.setPlaying(false);
            }
            if (isPlaying) {
                mPlayer.startPlayer(item);
                holder.setPlaying(true);
                lastHolder = holder;
                lastItemPlaying = item;
            } else {
                mPlayer.stopPlayer();
                holder.setPlaying(false);
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(EDIT_STATE_KEY, isEditMode);
        super.onSaveInstanceState(outState);
//        soundViewModel.setEditMode(isEditMode); // Make sure to store current editMode
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            soundViewModel.setEditMode(savedInstanceState.getBoolean(EDIT_STATE_KEY));
        }
    }

    // Fragment stop make, sure to stop player
    @Override
    public void onStop() {
        super.onStop();
        mPlayer.stopPlayer();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        soundViewModel.setEditMode(false);
    }

}
