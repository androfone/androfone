package com.example.androfone.filedialog;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.Rect;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.androfone.R;
import com.example.androfone.filedialog.utils.AudioUtils;
import com.example.androfone.filedialog.utils.SamplingUtils;
import com.example.androfone.filedialog.utils.TextUtils;

import java.util.LinkedList;

//import android.support.v4.content.ContextCompat;

/**
 * TODO: document your custom view class.
 */
public class WaveformView extends View {
    // Added interface
    public interface WaveformListener {
        public void onWaveformControlChanged(float x, float y);
    };

    private WaveformListener mListener;

    private static final String DEBUG_TAG = "WaveformView";

    public static final int MODE_RECORDING = 1;
    public static final int MODE_PLAYBACK = 2;

    private static final int HISTORY_SIZE = 6;

    private TextPaint mTextPaint;
    private Paint mStrokePaint, mRecordStrokePaint, mFillPaint, mMarkerPaint;

    // Used in draw
    private int brightness;
    private Rect drawRect;

    private int width, height;
    private float xStep, centerY;
    private int mMode, mAudioLength, mMarkerPosition, mSampleRate, mChannels;
    private short[] mSamples;
    private LinkedList<float[]> mHistoricalData;
    private Picture mCachedWaveform;
    private Bitmap mCachedWaveformBitmap;
    private int colorDelta = 255 / (HISTORY_SIZE + 1);
    private boolean showTextAxis = true;

    // Added for selection start - end
    private int mSelectionStart;
    private int mSelectionEnd;
    private float mStartPos = 0;
    private float mEndPos = 1;
    private Rect mSelectionRect;
    private int mSelectionColor;
    private int mBackgroundColor;
    private Paint mSelectionPaint;
    private GestureDetector mGestureDetector;
    private ScaleGestureDetector mScaleGestureDetector;
//    private WaveformListener mListener;
    private float mInitialScaleSpan;
    private Path mWaveform;
    private float mDensity;
    private int mOffsetL;
    private int mOffsetR;
    private float mOffset;


    public WaveformView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public WaveformView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public WaveformView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.WaveformView, defStyle, 0);

        mMode = a.getInt(R.styleable.WaveformView_mode, MODE_PLAYBACK);

        float strokeThickness = a.getFloat(R.styleable.WaveformView_waveformStrokeThickness, 1f);
        int mStrokeColor = a.getColor(R.styleable.WaveformView_waveformColor,
                ContextCompat.getColor(context, R.color.default_waveform));
        int mFillColor = a.getColor(R.styleable.WaveformView_waveformFillColor,
                ContextCompat.getColor(context, R.color.default_waveformFill));
        int mRecordColor = a.getColor(R.styleable.EditableWaveformView_waveformRecordColor,
                ContextCompat.getColor(context, R.color.default_record_waveform));
        int mMarkerColor = a.getColor(R.styleable.WaveformView_playbackIndicatorColor,
                ContextCompat.getColor(context, R.color.default_playback_indicator));
        int mTextColor = a.getColor(R.styleable.WaveformView_timecodeColor,
                ContextCompat.getColor(context, R.color.default_timecode));
        // Added for selection start - end
        mBackgroundColor = a.getColor(R.styleable.EditableWaveformView_backgroundColor,
                ContextCompat.getColor(context, R.color.default_background));
        mSelectionColor = a.getColor(R.styleable.EditableWaveformView_selectionColor,
                ContextCompat.getColor(context, R.color.default_selection));

        a.recycle();

        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
                mTextPaint.setColor(mTextColor);
        mTextPaint.setTextSize(TextUtils.getFontSize(getContext(),
                android.R.attr.textAppearanceSmall));

        mStrokePaint = new Paint();
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeWidth(strokeThickness);
        mStrokePaint.setAntiAlias(true);

        mRecordStrokePaint = new Paint();
        mRecordStrokePaint.setColor(mRecordColor);
        mRecordStrokePaint.setStyle(Paint.Style.STROKE);
        mRecordStrokePaint.setStrokeWidth(strokeThickness);
        mRecordStrokePaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mMarkerPaint = new Paint();
        mMarkerPaint.setStyle(Paint.Style.STROKE);
        mMarkerPaint.setStrokeWidth(0);
        mMarkerPaint.setAntiAlias(true);
        mMarkerPaint.setColor(mMarkerColor);

        // Added for selection start - end
        mSelectionPaint = new Paint();
        mSelectionPaint.setStyle(Paint.Style.FILL);
        mSelectionPaint.setColor(mSelectionColor);

        mGestureDetector = null;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        xStep = width / (mAudioLength * 1.0f);
        centerY = height / 2f;
        drawRect = new Rect(0, 0, width, height);
        // Added important mSelectionRect initialization
        mSelectionRect = new Rect((int)(mStartPos * width), 0, (int)mEndPos * width, height);
        // and also this that will be useful to convert event pos to dpi
        mDensity = getContext().getResources().getDisplayMetrics().density;
        // end Added important mSelectionRect initialization

        if (mHistoricalData != null) {
            mHistoricalData.clear();
        }
        if (mMode == MODE_PLAYBACK) {
            createPlaybackWaveform();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        LinkedList<float[]> temp = mHistoricalData;
        if (mMode == MODE_RECORDING && temp != null) {
            brightness = colorDelta;
            for (float[] p : temp) {
                mRecordStrokePaint.setAlpha(brightness);
                canvas.drawLines(p, mRecordStrokePaint);
                brightness += colorDelta;
            }
        } else if (mMode == MODE_PLAYBACK) {
            // Added to handle selection rectangle
            canvas.drawColor(mBackgroundColor);
            canvas.drawRect(mSelectionRect, mSelectionPaint);
            // end Added to handle selection rectangle
            if (mCachedWaveform != null) {
                canvas.drawPicture(mCachedWaveform);
            } else if (mCachedWaveformBitmap != null) {
                canvas.drawBitmap(mCachedWaveformBitmap, null, drawRect, null);
            }
            if (mMarkerPosition > -1 && mMarkerPosition < mAudioLength)
                canvas.drawLine(xStep * mMarkerPosition, 0, xStep * mMarkerPosition, height, mMarkerPaint);
        }
    }

    // Added for selection start - end
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                float x = event.getX();
                if (x > mSelectionRect.left -32 && x < mSelectionRect.right + 32) {
                    mOffsetL = (int) x - mSelectionRect.left;
                    mOffsetR = (int) x - mSelectionRect.right;
                    mOffset = x; // remember lastpos
                } else {
                    return false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
//                mListener.waveformTouchMove(event.getX());
                x = event.getX();
                if (x > mSelectionRect.left -32 && x < mSelectionRect.right + 32) {
                    updateSelectionRect(x);
                }
                mOffset = x;
//                Log.d(DEBUG_TAG,"x: " + event.getX() + " y: " + event.getY());
                break;
            case MotionEvent.ACTION_UP:
//                mListener.waveformTouchEnd();
                break;
        }
        return true;
    }

    // Added to handle selection rectangle
    public void updateSelectionRect(float pos) {

        int x = (int)(pos); // / mDensity);
        float dx = x - mOffset;

        if(Math.abs(mOffsetL) < 32) {
            mSelectionRect.left = (int) Math.max(0, Math.min(mSelectionRect.right-32, x));
            mSelectionRect.right = (int) Math.min(width, Math.max(mSelectionRect.right, x));
            mStartPos = mSelectionRect.left / width;
            mEndPos = mSelectionRect.right / width;
        } else if(Math.abs(mOffsetR) < 32) {
            mSelectionRect.right = (int) Math.min(width, Math.max(mSelectionRect.left+32, x));
            mSelectionRect.left = (int) Math.max(0, Math.min(mSelectionRect.left, x));
            mStartPos = mSelectionRect.left / width;
            mEndPos = mSelectionRect.right / width;
        } else {
            mSelectionRect.left = (int) Math.max(0, mSelectionRect.left + dx);
            mSelectionRect.right = (int) Math.min(width, mSelectionRect.right + dx);
//            Log.d(DEBUG_TAG,"x: " + x + " move both ");
        }
        // transform value for interface
        if( mListener != null ) {
            float range = (mSelectionRect.right - mSelectionRect.left) / (float)width;
            Log.d(DEBUG_TAG,"mSelectionRect.right: " + mSelectionRect.right + " - " +
                    "mSelectionRect.left " + mSelectionRect.left + " = range: " + range);
            float half = (mSelectionRect.right - mSelectionRect.left) / 2;
            float center = (mSelectionRect.left + half) / width;
            mListener.onWaveformControlChanged(center, range);
        }
        invalidate();
    } // end Added to handle selection rectangle

    // Added to direct handling of selection start - end
    public void setSelectionStart(float pos) {
        mSelectionRect.left = (int) (mStartPos = pos * width);
        invalidate();
//        Log.d(DEBUG_TAG,"WaveformView.setSelectionStart(float pos) pos: " + pos);
    }
    public void setSelectionEnd(float pos) {
        mSelectionRect.right = (int) (mEndPos = pos * width);
        invalidate();
    }
    // end Added to direct handling of selection start - end

    // Added to set interface
    public void setWaveformListener(WaveformListener listener) {
        mListener = listener;
    }

    public int getMode() {
        return mMode;
    }

    public void setMode(int mMode) {
        mMode = mMode;
    }

    public short[] getSamples() {
        return mSamples;
    }

    public void setSamples(short[] samples) {
        mSamples = samples;
        calculateAudioLength();
        onSamplesChanged();
    }

    public int getMarkerPosition() {
        return mMarkerPosition;
    }

    public void setMarkerPosition(int markerPosition) {
        mMarkerPosition = markerPosition;
        postInvalidate();
    }

    public int getAudioLength() {
        return mAudioLength;
    }

    public int getSampleRate() {
        return mSampleRate;
    }

    public void setSampleRate(int sampleRate) {
        mSampleRate = sampleRate;
        calculateAudioLength();
    }

    public int getChannels() {
        return mChannels;
    }

    public void setChannels(int channels) {
        mChannels = channels;
        calculateAudioLength();
    }

    public boolean showTextAxis() {
         return showTextAxis;
    }

    public void setShowTextAxis(boolean showTextAxis) {
        this.showTextAxis = showTextAxis;
    }

    private void calculateAudioLength() {
        if (mSamples == null || mSampleRate == 0 || mChannels == 0)
            return;

        mAudioLength = AudioUtils.calculateAudioLength(mSamples.length, mSampleRate, mChannels);
    }

    private void onSamplesChanged() {
        if (mMode == MODE_RECORDING) {
            if (mHistoricalData == null)
                mHistoricalData = new LinkedList<>();
            LinkedList<float[]> temp = new LinkedList<>(mHistoricalData);

            // For efficiency, we are reusing the array of points.
            float[] waveformPoints;
            if (temp.size() == HISTORY_SIZE) {
                waveformPoints = temp.removeFirst();
            } else {
                waveformPoints = new float[width * 4];
            }

            drawRecordingWaveform(mSamples, waveformPoints);
            temp.addLast(waveformPoints);
            mHistoricalData = temp;
            postInvalidate();

        } else if (mMode == MODE_PLAYBACK) {
            mMarkerPosition = -1;
            xStep = width / (mAudioLength * 1.0f);
            createPlaybackWaveform();
        }
    }

    void drawRecordingWaveform(short[] buffer, float[] waveformPoints) {
        float lastX = -1;
        float lastY = -1;
        int pointIndex = 0;
        float max = Short.MAX_VALUE;

        // For efficiency, we don't draw all of the samples in the buffer, but only the ones
        // that align with pixel boundaries.
        for (int x = 0; x < width; x++) {
            int index = (int) (((x * 1.0f) / width) * buffer.length);
            short sample = buffer[index];
            float y = centerY - ((sample / max) * centerY);

            if (lastX != -1) {
                waveformPoints[pointIndex++] = lastX;
                waveformPoints[pointIndex++] = lastY;
                waveformPoints[pointIndex++] = x;
                waveformPoints[pointIndex++] = y;
            }

            lastX = x;
            lastY = y;
        }
    }

    Path drawPlaybackWaveform(int width, int height, short[] buffer) {
        Path waveformPath = new Path();
        float centerY = height / 2f;
        float max = Short.MAX_VALUE;

        short[][] extremes = SamplingUtils.getExtremes(buffer, width);


        waveformPath.moveTo(0, centerY);

        // draw maximums
        for (int x = 0; x < width; x++) {
            short sample = extremes[x][0];
            float y = centerY - ((sample / max) * centerY);
            waveformPath.lineTo(x, y);
        }

        // draw minimums
        for (int x = width - 1; x >= 0; x--) {
            short sample = extremes[x][1];
            float y = centerY - ((sample / max) * centerY);
            waveformPath.lineTo(x, y);
        }

        waveformPath.close();

        return waveformPath;
    }

    private void createPlaybackWaveform() {
        if (width <= 0 || height <= 0 || mSamples == null)
            return;

        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedWaveform = new Picture();
            cacheCanvas = mCachedWaveform.beginRecording(width, height);
        } else {
            mCachedWaveformBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedWaveformBitmap);
        }

//        mSelectionRect.set(90, 0, 600, height);
//        cacheCanvas.drawColor(mBackgroundColor);
//        cacheCanvas.drawRect(mSelectionRect, mSelectionPaint);
        mWaveform = drawPlaybackWaveform(width, height, mSamples);
        cacheCanvas.drawPath(mWaveform, mFillPaint);
        cacheCanvas.clipPath(mWaveform);
//        cacheCanvas.drawPath(mWaveform, mStrokePaint);
//        drawAxis(cacheCanvas, width);

        if (mCachedWaveform != null)
            mCachedWaveform.endRecording();
    }

    private void drawAxis(Canvas canvas, int width) {
        if (!showTextAxis) return;
        int seconds = mAudioLength / 1000;
        float xStep = width / (mAudioLength / 1000f);
        float textHeight = mTextPaint.getTextSize();
        float textWidth = mTextPaint.measureText("10.00");
        int secondStep = (int)(textWidth * seconds * 2) / width;
        secondStep = Math.max(secondStep, 1);
        for (float i = 0; i <= seconds; i += secondStep) {
            canvas.drawText(String.format("%.2f", i), i * xStep, textHeight, mTextPaint);
        }
    }
}
