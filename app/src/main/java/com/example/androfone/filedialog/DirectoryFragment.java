package com.example.androfone.filedialog;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androfone.R;

import java.util.ArrayList;
import java.util.List;


public class DirectoryFragment extends Fragment {

    private static final String DEBUG_TAG = "DirectoryFragment";


    private SoundViewModel soundViewModel;
    public Long curDirectoryId;

    public interface OnItemSelectedListener {
        void onItemSelected(String name, long id);
    }
    private OnItemSelectedListener mListener;

    public DirectoryFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        return inflater.inflate(R.layout.fd_fragment_directory, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = view.findViewById(R.id.directory_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DirectoryAdapter dadapter = new DirectoryAdapter();
        recyclerView.setAdapter(dadapter);
        soundViewModel = new ViewModelProvider(requireActivity()).get(SoundViewModel.class);
        soundViewModel.getLiveDirectoryList().observe(getViewLifecycleOwner(), directoryItems -> {
            Log.d(DEBUG_TAG,"observing directory list");
            Log.d(DEBUG_TAG,"list length: " + directoryItems.size());

            dadapter.setDirectories(directoryItems);
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof OnItemSelectedListener) {
            mListener = (OnItemSelectedListener) getParentFragment();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement DirectoryFragment.OnItemSelectedListener");
        }
    }

    public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryHolder> {

        private List<DirectoryItem> directories = new ArrayList<>();

        @NonNull
        @Override
        public DirectoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fd_fragment_directory_item, parent, false);
            return new DirectoryHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull DirectoryHolder holder, int position) {
            DirectoryItem curDir = directories.get(position);
            final String name = curDir.getName();
            holder.name.setText(name);
            holder.name.setOnClickListener(v -> {
                // this set the cur directory used in
                // viewmodel.getLiveSoundByDirectory in file fragment
                curDirectoryId = curDir.getId();
                soundViewModel.setDirectoryId(curDir.getId());
                if (mListener != null) {
                    Log.d(DEBUG_TAG,"item clicked: " + name);
                    mListener.onItemSelected(name, curDirectoryId);

                }
            });
        }

        @Override
        public int getItemCount() {
            return directories.size();
        }

        public void setDirectories(List<DirectoryItem> dirs) {
            this.directories = dirs;
            notifyDataSetChanged();
        }

        class DirectoryHolder extends RecyclerView.ViewHolder { //implements View.OnClickListener {
            final TextView name;
//            private TextView id;

            public DirectoryHolder(@NonNull View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.text);
//                itemView.setOnClickListener(this);
//                id = itemView.findViewById(R.id.id);
            }

//            @Override
//            public void onClick(View v) {
//                String name = directories.get(getAdapterPosition()).getName();
//
//                if (mListener != null) {
//                    mListener.onItemSelected(name, getAdapterPosition());
//                    Log.d(DEBUG_TAG,"item clicked: " + name);
//                }
//            }
        }

    }
}
