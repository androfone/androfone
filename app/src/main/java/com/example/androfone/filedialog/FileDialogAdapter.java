package com.example.androfone.filedialog;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androfone.R;
import com.example.androfone.filedialog.utils.PreviewPlayer;

public class FileDialogAdapter extends ListAdapter<SoundItem, FileDialogAdapter.SoundHolder> {
    private static final String DEBUG_TAG = "FileAdapter";
    public static final int FILE_ADAPTER = 0;
    public static final int USER_ADAPTER = 1;
    public static final int FAVORITES_ADAPTER = 2;
    private final int mAdapterType;

    private final FileFragment.OnItemSelectedListener itemSelectedListener;

    public interface OnPlayPreviewListener {
        void onPlayPreview(FileDialogAdapter.SoundHolder holder);
    }
    private FileDialogAdapter.OnPlayPreviewListener playPreviewListener;

    public void setOnPlayPreviewListener(FileDialogAdapter.OnPlayPreviewListener listener) {
        this.playPreviewListener = listener;
    }
//    private boolean isActive = false;
//    private boolean isPlaying = false;

    public interface OnFavoriteChangedListener {
        void onFavoriteChanged(SoundItem item);
    }
    private FileDialogAdapter.OnFavoriteChangedListener favoriteChangedListener;

    public void setOnFavoriteChangedListener(FileDialogAdapter.OnFavoriteChangedListener listener) {
        this.favoriteChangedListener = listener;
    }

    public PreviewPlayer mPlayer;

    public FileDialogAdapter(int adapterType, FileFragment.OnItemSelectedListener listener) {
        super(DIFF_CALLBACK);
        this.itemSelectedListener = listener;
        this.mAdapterType = adapterType;
    }
    public boolean isEditMode = false;

    private static final DiffUtil.ItemCallback<SoundItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<SoundItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull SoundItem oldItem, @NonNull SoundItem newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull SoundItem oldItem, @NonNull SoundItem newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getUri().equals(newItem.getUri()) &&
                    oldItem.isFavorite() == newItem.isFavorite() &&
                    oldItem.getTag() == newItem.getTag();
        }
    };

    @NonNull
    @Override
    public FileDialogAdapter.SoundHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        final FileDialogAdapter.SoundHolder holder = new SoundHolder(itemView);
        holder.itemView.setOnClickListener(v -> {
            int position = holder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                itemSelectedListener.onItemSelected(getItem(position), position);
            }
        });
        holder.playpreviewFrame.setOnClickListener(v -> {
            int position = holder.getAdapterPosition();
            if (playPreviewListener != null && position != RecyclerView.NO_POSITION) {
                SoundItem item = getItem(position);
                playPreviewListener.onPlayPreview(holder);
            }
        });
        if (favoriteChangedListener != null) {
            Log.d(DEBUG_TAG,"favoriteChangedListener != null");
            holder.isFavoriteFrame.setVisibility(View.VISIBLE);
            holder.isFavorite.setOnClickListener(v -> {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    SoundItem item = getItem(position);
                    boolean isChecked = !item.isFavorite();
                    holder.isFavorite.setChecked(isChecked);
                    item.setFavorite(isChecked);
                    Log.d(DEBUG_TAG,"sound favorite button onclickListener name: "
                            + item.getName() + " favorite = " + isChecked);
                    favoriteChangedListener.onFavoriteChanged(item);
                }
            });
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull FileDialogAdapter.SoundHolder holder, int position) {
        SoundItem curSound = getItem(position);
        if (mAdapterType != FILE_ADAPTER) {
            holder.sweep.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        }
        holder.name.setText(curSound.getName());
        if (mAdapterType != FAVORITES_ADAPTER) {
            holder.isFavorite.setChecked(curSound.isFavorite());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.fd_fragment_sound_item;
    }

    public SoundItem getItemAt(int pos) {
        return getItem(pos);
    }

    static class SoundHolder extends RecyclerView.ViewHolder {
        private boolean isActive = false;
        private final AppCompatImageView sweep;
        private final FrameLayout playpreviewFrame;
        private final AppCompatImageView playpreview;
        private final TextView name;
        private final FrameLayout isFavoriteFrame;
        private final RadioButton isFavorite;

        public SoundHolder(@NonNull View itemView) {
            super(itemView);
            sweep = itemView.findViewById(R.id.sweep_delete_icon);
            playpreviewFrame = itemView.findViewById(R.id.sound_item_playpreview_button_frame);
            playpreview = itemView.findViewById(R.id.sound_item_playpreview_button);
            name = itemView.findViewById(R.id.sound_item_text);
            isFavoriteFrame = itemView.findViewById(R.id.sound_item_favorite_marker_frame);
            isFavorite = itemView.findViewById(R.id.sound_item_favorite_marker);
        }

        public boolean isPlaying() {
            return isActive;
        }

        public void setPlaying(boolean playing) {
            isActive = playing;
            playpreview.setActivated(playing);
        }
    }

}
