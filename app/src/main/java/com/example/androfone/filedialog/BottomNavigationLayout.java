package com.example.androfone.filedialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

// This class implement the bottom navigation tabs
public class BottomNavigationLayout extends LinearLayout implements View.OnClickListener{

    public interface BottomNavigationListener {
        void onTabSelected(int position);
    }
    private BottomNavigationListener mListener;

    private View mCurView;
    private int mSelected;

    public BottomNavigationLayout(Context context) {
        super(context);
    }

    public BottomNavigationLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setBottomNavigationListener(BottomNavigationListener listener) {
        mListener = listener;
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);
        child.setOnClickListener(this);
    }

    public void setSelected(int position) {
        View v = getChildAt(position);
        mSelected = position;
        onClick(v);
    }

    public int getSelected() {
        return mSelected;
    }


    @Override
    public void onClick(View v) {
        if ((mCurView != null) && (mCurView != v)) {
            mCurView.setSelected(false);
        }
        v.setSelected(true);
        mCurView = v;
        mSelected = indexOfChild(mCurView);
        if (mListener != null) {
            mListener.onTabSelected(indexOfChild(v));
        }
    }
}
