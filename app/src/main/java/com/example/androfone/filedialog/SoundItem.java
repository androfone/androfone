package com.example.androfone.filedialog;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.net.URI;

import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = "sound_item_table", foreignKeys = @ForeignKey(
        entity = DirectoryItem.class,
        parentColumns = "directory_id",
        childColumns = "directory_id",
        onDelete = CASCADE),
        indices = {@Index(value = {"directory_id"})})
public class SoundItem {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "directory_id")
    private long directoryId;

    private String name;

    private URI uri;

    private boolean isFavorite;

    private int tag;

    public SoundItem(String name, URI uri, long directoryId,
                     boolean isFavorite, int tag) {
        this.name = name;
        this.uri = uri;
        this.directoryId = directoryId;
        this.isFavorite = isFavorite;
        this.tag = tag;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(long directoryId) {
        this.directoryId = directoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getTag() { return tag; }

    public void setTag(int tag) { this.tag = tag; }

    public class Tag {
        public int INTERNAL = 0;
        public int USER = 1;
        public int EXTERNAL = 2;
    }
}
