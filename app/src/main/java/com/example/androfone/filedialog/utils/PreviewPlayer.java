package com.example.androfone.filedialog.utils;

import android.util.Log;

import com.example.androfone.filedialog.SoundItem;
import com.example.androfone.filedialog.SoundRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class PreviewPlayer {

    private static final String DEBUG_TAG = PlaybackThread.class.getSimpleName();

    private final File mRoot;
    private PlaybackThread mPlaybackThread;
    private final PlaybackListener mPlaybackListener;
    private boolean isPlayerRunning = false;


    public PreviewPlayer(PlaybackListener listener) {
        this.mRoot = SoundRepository.getFileDirectory();
        this.mPlaybackListener = listener;
    }

    public boolean isPlayerRunning() {
        return isPlayerRunning;
    }

    public void startPlayer(SoundItem item) {
        Log.d(DEBUG_TAG, "startPlayer item " + item.getName());

        isPlayerRunning = true;

        String uri = item.getUri().getPath();
        Log.d(DEBUG_TAG, "startPlayer item uri " + uri);
        Log.d(DEBUG_TAG, "full item name " + mRoot + File.separator + uri);

        File f = new File(mRoot, uri);
        short[] audio = new short[0];
        if (f.exists()) {
            try {
                audio = getAudioSample(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mPlaybackThread != null) {
            mPlaybackThread.stopPlayback();
            mPlaybackThread = null;
        }
        if (audio.length != 0) {
            mPlaybackThread = new PlaybackThread(mPlaybackListener);
            mPlaybackThread.setSamples(audio);
            mPlaybackThread.startPlayback();
        } else Log.d(DEBUG_TAG, "No sound to play");
    }

    public void stopPlayer() {
        if (mPlaybackThread != null) {
            mPlaybackThread.stopPlayback();
        }
        mPlaybackThread = null;
        isPlayerRunning = false;
    }

    private short[] getAudioSample(File f) throws IOException {
        Log.d(DEBUG_TAG, "in getAudioSample()");
        byte[] data;
        int size = (int) f.length();// - 44; // length minus header length
        data = new byte[size];
        byte[] tmpBuff = new byte[size];
        try (FileInputStream fis = new FileInputStream(f)) {
            int read = fis.read(data, 0, size);
                if (read < size) {
                    int remain = size - read;
                    while (remain > 0) {
                        read = fis.read(tmpBuff, 0, remain);
                        System.arraycopy(tmpBuff, 0, data, size - remain, read);
                        remain -= read;
                    }
                }
        }

        ShortBuffer sb = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        short[] samples = new short[sb.limit()];
        sb.get(samples);
        return samples;
    }

}
