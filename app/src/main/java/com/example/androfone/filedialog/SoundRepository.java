package com.example.androfone.filedialog;

import android.app.Application;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class SoundRepository {

    private static final String DEBUG_TAG = "SoundRepository";

    private final SoundItemDao soundItemDao;
    private final DirectoryItemDao directoryItemDao;
    private final LiveData<List<SoundItem>> internalSoundList;
    private final LiveData<List<SoundItem>> userSoundList;
    private final LiveData<List<SoundItem>> favoritesList;

    private final LiveData<List<DirectoryItem>> directoryList;
    private DirectoryItem userDirectory;

    private static File filesDirectory;
    public static File getFileDirectory() { return filesDirectory; }

    public interface SetUserDirectoryCB {
        void setUserDirectory(DirectoryItem directory);
    }

    public SoundRepository(Application application) {

        /////////////////////////////////////////////////////////
        // for when we'll find where we have put many deprecated asyncTask:
        // Executors.newSingleThreadScheduledExecutor().execute { //here }
        /////////////////////////////////////////////////////////

        filesDirectory = application.getFilesDir();
        File f = new File(filesDirectory, "sound bank");
        // if directory list is empty the database have not been populated yet
        // populate the database and create user directory on local storage
        if (!f.exists()) {
            Log.d(DEBUG_TAG,"list is empty, populate");
            populateDefaultFiles(application);
        }

        // get the database and set userDirectory through the callback
        Log.d(DEBUG_TAG,"initialize database");
        SoundDatabase database = SoundDatabase.getInstance(application, directory -> {
            userDirectory = directory;
            Log.d(DEBUG_TAG, "SoundRepository(Application application) userDirectoryCB returned: " + userDirectory.getUri().getPath());
        });
        soundItemDao = database.soundItemDao();
        directoryItemDao = database.directoryItemDao();

        directoryList = directoryItemDao.getDirectoryList();

        internalSoundList = soundItemDao.getInternalSoundList();
        userSoundList = soundItemDao.getUserSoundList();
        favoritesList = soundItemDao.getFavoriteList();
    }

    public void insert(DirectoryItem item) {
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            directoryItemDao.insert(item);
        });
    }

    public void insert(SoundItem item) {
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            soundItemDao.insert(item);
        });
    }

    public void update(SoundItem item) {
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            soundItemDao.update(item); // ideally put the rename code here
        });
    }

    public void delete(SoundItem item) {
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            soundItemDao.delete(item);
            String name = item.getName();
            File f = new File(filesDirectory, item.getUri().getPath());
            if (!f.delete()) Log.d(DEBUG_TAG,"could not delete file " + item.getName());
            else Log.d(DEBUG_TAG,"file <" + name + "> deleted!");
        });
    }

    public void renameSoundFile(String newName, SoundItem item) {
        SoundItem renamedItem = rename(newName, item);
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            soundItemDao.update(renamedItem);
        });
    }

    public void createNewSoundItem(String name, File file) {
        SoundItem item = saveSoundFile(name, file); // Create a new sound item from file
        SoundDatabase.databaseWriteExecutor.execute(() -> {
            soundItemDao.insert(item); // this is where a new recorded sound get in the database
        });
    }

    public LiveData<List<SoundItem>> getInternalSoundList() {
        return internalSoundList;
    }

    public LiveData<List<SoundItem>> getSoundsByDirectory(long directoryId) {
        MutableLiveData<List<SoundItem>> list = new MutableLiveData<>();
        ArrayList<SoundItem> soundInDir = new ArrayList<>();
        for (SoundItem sound: Objects.requireNonNull(internalSoundList.getValue())) {
            if (sound.getDirectoryId() == directoryId) {
                soundInDir.add(sound);
            }
        }
        list.setValue(soundInDir);
        return list;
    }

    public LiveData<List<SoundItem>> getUserSoundList() {
        return userSoundList;
    }

    public LiveData<List<SoundItem>> getFavorites() {
        return favoritesList;
    }

    public LiveData<List<DirectoryItem>> getDirectoryList() {
        return directoryList;
    }

    public DirectoryItem getUserDirectory() { return userDirectory; }



    private void populateDefaultFiles(Application application) {

        String rootDir = "sound bank";
        String userDir = "user sounds";
        AssetManager am = application.getAssets();
        String destPath = application.getFilesDir().getPath() + File.separator + rootDir;
        Log.d(DEBUG_TAG,"destPath: " + destPath);
        try {
            copyAssets(am, rootDir, destPath);
        } catch (IOException e){
            Log.d(DEBUG_TAG,"Exeption!");
            e.printStackTrace();
        }

        // create user directory
        Log.d(DEBUG_TAG,"creating user dir");
        String userpath = application.getFilesDir().getPath() + File.separator + userDir;
        File user = new File(userpath);
        if (!user.exists()) {
            if (user.mkdirs()) {
                Log.d(DEBUG_TAG,"user Directory is created!");
            } else {
                Log.d(DEBUG_TAG,"Failed to create user directory!");
            }
        }

        Log.d(DEBUG_TAG,"populate files finished");
    }

    private void copyAssets(AssetManager am, String source, String dest) throws IOException {

        String[] list = am.list(source);

        for (String name : list) {
            String sourcepath = source + File.separator + name;
            String destpath = dest + File.separator + name;

            AtomicReferenceArray<String> sublist = new AtomicReferenceArray<>(am.list(sourcepath));
            if (sublist.length() != 0) { // its a directory
                // create directory and recurse
                Log.d(DEBUG_TAG,"create directory: " + destpath);
                // name minus the first three digits characters
                destpath = dest + File.separator + name.substring(3);
                createDirectory(destpath);
                copyAssets(am, sourcepath, destpath);

            } else { // assuming its a file but possible to be an empty directory - find reliable test
                if (name.endsWith(".wav")) { // can still be a directory which name ending with .wav
                    // copy file from pathname
                    Log.d(DEBUG_TAG,"path: " + destpath);
                    createCopy(am, sourcepath, destpath);
                }
            }
        }
    }

    private static void createDirectory(String directoryPath) {
        File file = new File(directoryPath);
        if (!file.exists()) {
            if (file.mkdirs()) {
                Log.d(DEBUG_TAG,"Directory is created!");
            } else {
                Log.d(DEBUG_TAG,"Failed to create directory!");
            }
        }
    }

    private void createCopy(AssetManager am, String source, String dest) throws IOException {
        InputStream in = am.open(source);
        OutputStream out = new FileOutputStream(dest);

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.close();
    }

    private SoundItem rename(String newName, SoundItem item) {
        String oldName = item.getName();
        if (!newName.endsWith(".wav")) newName = newName + ".wav";
        String name = newName;
        DirectoryItem base = getUserDirectory();
        String root = filesDirectory.getPath();
        Log.d(DEBUG_TAG, "new file name " + root + File.separator + base.getUri().getPath() + name);
        String userpath = root + File.separator + base.getUri().getPath();
        Log.d(DEBUG_TAG, "userpath: " + userpath);

        File from = new File(userpath, oldName + ".wav");
        File to = new File(userpath, newName);
        if (from.exists()) {
            Log.d(DEBUG_TAG, "from.exists");
            if (from.renameTo(to)) {
                // File have been renamed, we can rename item and change its URI
                String itemName = to.getName().substring(0, to.getName().length() - 4);
                item.setName(itemName);
                item.setUri(filesDirectory.toURI().relativize(to.toURI()));

                Log.d(DEBUG_TAG, "Success! file " + oldName + " renamed " + to);
                Log.d(DEBUG_TAG, "SoundItem name: " + item.getName() + " URI: " + item.getUri());
            } else Log.d(DEBUG_TAG, "Rename FAILED!");
        }
        return item;
    }

    private SoundItem saveSoundFile(String result, File newfile)  {
        Log.d(DEBUG_TAG, "input name: " + result);

        String filename = result + ".wav";
        DirectoryItem base = getUserDirectory();
        Log.d(DEBUG_TAG, "setFile() userDirectory: " + base.getUri().getPath());
        String root = filesDirectory.getPath();

        Log.d(DEBUG_TAG, "new file name " + root + File.separator + base.getUri().getRawPath() + filename);
        String userpath = root + File.separator + base.getUri().getPath();

        File from = new File(root, newfile.getName());
        File to = new File(userpath, filename);
        try {
            copyFilePlainJava(from.getPath(), to.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(DEBUG_TAG, "new file name " + to.getName() + " size " + to.length());

        SoundItem s = new SoundItem(result, filesDirectory.toURI().relativize(to.toURI()),
                base.getId(), false, 1);
        Log.d(DEBUG_TAG, "new SoundItem name " + s.getName() + " uri: " + s.getUri() +
                " dirId: " + s.getDirectoryId() + " favorite? " + s.isFavorite() + " tag: " + s.getTag());
        if(from.delete()) Log.d(DEBUG_TAG, "from successfully deleted");
        return s;
    }

    public static void copyFilePlainJava(String from, String to) throws IOException {

        InputStream inStream = null;
        OutputStream outStream = null;

        try {

            File fromFile = new File(from);
            File toFile = new File(to);

            inStream = new FileInputStream(fromFile);
            outStream = new FileOutputStream(toFile);

            byte[] buffer = new byte[1024];

            int length;
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
                outStream.flush();
            }

        } finally {
            if (inStream != null)
                inStream.close();

            if (outStream != null)
                outStream.close();
        }
    }
}
