/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.androfone.filedialog.utils;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.example.androfone.MainActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;


public class RecordingThread {

    private static final String LOG_TAG = RecordingThread.class.getSimpleName();
    private static final int SAMPLE_RATE = MainActivity.getSampleRate();

    private File mFile;
    private OutputStream mOutputStream;
    private boolean mShouldContinue;
    private boolean isRecording = false;
    private final AudioDataReceivedListener mListener;
    private Thread mThread;
    private long totalSample = 0L;

    public RecordingThread(AudioDataReceivedListener listener) {
        Log.d(LOG_TAG, "/// RecordingThread /// Constructor");
        this.mListener = listener;
        mOutputStream = null;
    }

    public void startSafe() {
        Log.d(LOG_TAG, "startSafe() - start continuous recording");
        Log.d(LOG_TAG, "startSafe() - about to set mShouldContinue = true");
        mShouldContinue = true;
        mThread = new Thread(() -> {
            try {
                recordContinuous();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Log.d(LOG_TAG, "startSafe() - about to set mThread.start()");
        mThread.start();
    }

    public void stopSafe() {
        Log.d(LOG_TAG, "stopSafe() - stop continuous recording");
        if (mThread == null)
            return;
        if (isRecording)
            stopRecording();
        Log.d(LOG_TAG, "stopSafe() - about to set mShouldContinue = false");
        mShouldContinue = false;
        Log.d(LOG_TAG, "stopSafe() - about to set mThread = null");
        mThread = null;
    }

    public void setFile(File file) {
        Log.d(LOG_TAG, "setFile(File file)");
        this.mFile = file;
        mOutputStream = getOutputStream(file);
    }

    public void startRecording() {
        Log.d(LOG_TAG, "startRecording()");
        if (mThread != null) {
            if (!isRecording) {// Make sure we don't start a started recording
                Log.d(LOG_TAG, "isRecording = true");
                isRecording = true;

            }
        }
    }

    public void stopRecording() {
        Log.d(LOG_TAG, "stopRecording()");

        if (isRecording) { // Make sure we dont stop a stopped recording
            isRecording = false;
            try {
                mOutputStream.flush();
                mOutputStream.close();
                mOutputStream = null;
                writeWavHeader();
            } catch (IOException e) {
                e.printStackTrace();
            }
            totalSample = 0L; // Reset counter
        }
    }

    private void recordContinuous() throws IOException {
        Log.v(LOG_TAG, "recordContinuous()");
        // Setup our thread priority, buffer size, audio buffer and AudioRecord instance
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            // Hope this won't happen as its a lot
            // We should have a way to handle the case in which the recording be smaller
            // than this buffer size
            bufferSize = SAMPLE_RATE * 2;
        }
        Log.d(LOG_TAG, "recordContinuous() - buffer size " + bufferSize);


        short[] audioBuffer = new short[bufferSize / 2];

        AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);

        if (audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(LOG_TAG, "recordContinuous() - Audio Record can't initialize!");
            return;
        }

        // start AudioRecord
        audioRecord.startRecording();
        Log.v(LOG_TAG, "recordContinuous() - AudioRecord.startRecording()");

        long sampleRead;
        while (mShouldContinue) {
            sampleRead = audioRecord.read(audioBuffer, 0, audioBuffer.length);

            // Skip out until we are actually recording
            if (!isRecording) continue;

            // Write to file
            mOutputStream.write(shortToBytes(audioBuffer, audioBuffer.length));
            // Notify waveform
            totalSample += sampleRead;
            mListener.onAudioDataReceived(audioBuffer, totalSample);
        }

        audioRecord.stop();
        Log.v(LOG_TAG, "recordContinuous() - AudioRecord.stop()");
        audioRecord.release();
    }

    public byte[] shortToBytes(short[] shortBuffer, int numberOfShorts) {
        int shortIndex, byteIndex;
        byte[] buffer = new byte[numberOfShorts * 2];
        shortIndex = byteIndex = 0;
        while (shortIndex != numberOfShorts) {
            buffer[byteIndex] = (byte) (shortBuffer[shortIndex] & 0x00FF);
            buffer[byteIndex + 1] = (byte) ((shortBuffer[shortIndex] & 0xFF00) >> 8);
            ++shortIndex;
            byteIndex += 2;
        }
        return buffer;
    }

    private OutputStream getOutputStream(File file) {
        if (file == null) {
            throw new RuntimeException("file is null !");
        }
        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(
                    "could not build OutputStream from" + " this file " + file.getName(), e);
        }
        return outputStream;
    }

    private void writeWavHeader() throws IOException {
        final RandomAccessFile wavFile = randomAccessFile(mFile);
        wavFile.seek(0); // to the beginning
        wavFile.write(new WavHeader(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, mFile.length()).toBytes());
        wavFile.close();
    }

    private RandomAccessFile randomAccessFile(File file) {
        RandomAccessFile randomAccessFile;
        try {
            randomAccessFile = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return randomAccessFile;
    }


}
