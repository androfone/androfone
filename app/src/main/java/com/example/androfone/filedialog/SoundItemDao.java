package com.example.androfone.filedialog;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SoundItemDao {

    @Insert
    void insert(SoundItem item);

    @Update
    void update(SoundItem item);

    @Delete
    void delete(SoundItem item);

    @Query("DELETE FROM sound_item_table")
    void deleteAll();

    @Query("SELECT * FROM sound_item_table WHERE tag = 0 ORDER BY name ASC")
    LiveData<List<SoundItem>> getInternalSoundList();

    @Query("SELECT * FROM sound_item_table WHERE tag = 1 ORDER BY name ASC")
    LiveData<List<SoundItem>> getUserSoundList();

    @Query("SELECT * FROM sound_item_table WHERE isFavorite = 1 ORDER BY name ASC")
    LiveData<List<SoundItem>> getFavoriteList();

    // Keep this as it can be handy for other constructs
//    @Query("SELECT EXISTS (SELECT 1 FROM sound_item_table WHERE name = :name AND tag =1)")
//    LiveData<Boolean> isUserSoundItemExist(String name);
}
