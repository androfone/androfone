package com.example.androfone.filedialog;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.net.URI;

//@Entity(tableName = "directory_item_table", indices = {@Index(value = {"directory_id"}, unique = true)}) // TEST
@Entity(tableName = "directory_item_table")
public class DirectoryItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "directory_id") // TEST
    private long id;

    private String name;

    private URI uri;

    private int listNumber;

    private boolean isReadOnly;

    public DirectoryItem(String name, URI uri, int listNumber, boolean isReadOnly) {
        this.name = name;
        this.uri = uri;
        this.listNumber = listNumber;
        this.isReadOnly = isReadOnly;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public URI getUri() {
        return uri;
    }

    public void setListNumber(int listNumber) {
        this.listNumber = listNumber;
    }

    public int getListNumber() {
        return listNumber;
    }

    public void setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
    }

    public boolean getIsReadOnly() {
        return isReadOnly;
    }
}
