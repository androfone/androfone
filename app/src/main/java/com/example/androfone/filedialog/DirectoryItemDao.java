package com.example.androfone.filedialog;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DirectoryItemDao {

    @Insert
    void insert(DirectoryItem item);

    @Query("DELETE FROM directory_item_table")
    void deleteAll();

    @Query("SELECT * FROM directory_item_table WHERE isReadOnly = 1 ORDER BY listNumber ASC")
    LiveData<List<DirectoryItem>> getDirectoryList();

    @Query("SELECT * FROM directory_item_table WHERE isReadOnly = 0 LIMIT 1")
    DirectoryItem getUserDirectory();

    @Query("SELECT * FROM directory_item_table WHERE name = :dirname")
    DirectoryItem getDirectory(String dirname);

}
