package com.example.androfone.filedialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.androfone.R;

public class TextInputDialogFragment extends DialogFragment {

    public static final String DEBUG_TAG = "TextInputDialogFragment";
    public static final String INPUT_NAME_KEY = "input_name";

    public interface OnOKButtonClickListener {
        void onOKButtonClick(TextInputDialogFragment dialog);
    }

    private OnOKButtonClickListener onOKButtonClickListener;

    private EditText mInputView;
    private TextView mSaveButton;
    private String mOldName;
    private boolean isSaveDialog = true;

    public TextInputDialogFragment() {
    }

    public static TextInputDialogFragment newInstance(@Nullable String inputName) {
        TextInputDialogFragment dialog = new TextInputDialogFragment();
        Bundle args = new Bundle();

        args.putString(INPUT_NAME_KEY, inputName);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the parent fragment implements the callback interface
        try {
            // Instantiate the OnOKButtonClickListener so we can send events to the host
            onOKButtonClickListener = (OnOKButtonClickListener) getParentFragment();
        } catch (ClassCastException e) {
            // The fragment doesn't implement the interface, throw exception
            throw new ClassCastException(getParentFragment().toString()
                    + " must implement OnOKButtonClickListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate(@Nullable Bundle savedInstanceState) called");
        setRetainInstance(true);
    }

    @SuppressLint("InflateParams")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.fd_fragment_text_input_dialog, null);
        Log.d(DEBUG_TAG, "view have been inflated saveButton" + mSaveButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        TextView title = view.findViewById(R.id.title);
        Log.d(DEBUG_TAG, "mTitle " + title);
        TextView discardButton = view.findViewById(R.id.discard_button);
        discardButton.setOnClickListener(v -> dismiss());
        mSaveButton = view.findViewById(R.id.save_button);
        Log.d(DEBUG_TAG, "mSaveButton " + mSaveButton);
        mSaveButton.setEnabled(false);
        mInputView = view.findViewById(R.id.input_text);
        Log.d(DEBUG_TAG, "mInputView " + mInputView);

        if (getArguments() != null) {
            mOldName = getArguments().getString(INPUT_NAME_KEY);
            Log.d(DEBUG_TAG, "mOldName " + mOldName);
            if (mOldName.equals("")) {
                isSaveDialog = true;
            } else {
                isSaveDialog = false;
                mInputView.setText(mOldName);
                mInputView.setSelection(mOldName.length());
            }
        }

        Log.d(DEBUG_TAG, "isSaveDialog " + isSaveDialog);
        if (isSaveDialog) {
            title.setText(R.string.title_name);
        } else {
            title.setText(R.string.title_rename);
        }


        Log.d(DEBUG_TAG, "oldName" + mOldName);
        String input = mInputView.getText().toString();
        Log.d(DEBUG_TAG, "input" + input);
        mInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSaveButton.setEnabled(s.toString().trim().length() != 0 &&
                        !s.toString().contentEquals(input));
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mSaveButton.setOnClickListener(v -> {
            if (mSaveButton.isEnabled()) {
                onOKButtonClickListener.onOKButtonClick(this);
            }
        });


        return dialog;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        setShowsDialog(true);
    }

    public EditText getInputView() {
        return mInputView;
    }

    public boolean isSaveDialog() {
        return isSaveDialog;
    }
}
