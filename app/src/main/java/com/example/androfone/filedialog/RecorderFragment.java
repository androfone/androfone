package com.example.androfone.filedialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.androfone.EditableWaveformView;
import com.example.androfone.R;
import com.example.androfone.filedialog.utils.RecordingThread;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;

public class RecorderFragment extends Fragment {


    private static final String DEBUG_TAG = RecorderFragment.class.getSimpleName();
    private static final int REQUEST_RECORD_AUDIO = 13;

    private File mRootDirectory;
    private EditableWaveformView mRecordView;
    private RecordButton2 mRecordButton;
    public SoundViewModel mSoundViewModel;
    RecordingThread mRecordingThread;
    private File mFile;
    private File transientFile;
    private boolean isActive = false;

    // This interface is set in UserFragment
    public interface OnRecordEndedListener {
        void onEnded(File file);
    }
    private OnRecordEndedListener mListener;
    public void setOnRecordFinishedListener(OnRecordEndedListener listener) {
        this.mListener = listener;
    }

    RecorderFragment() {
        Log.d(DEBUG_TAG, "//// RecorderFragment() //// Constructor");
        mRecordingThread = new RecordingThread((data, total)-> {
            mRecordView.setSamples(data);
            float progress = (float)total / (float)(48000*4);
            mSoundViewModel.setRecordProgress(progress);
            if (total > (48000 * 4)) {
                isActive = false;
                mSoundViewModel.setIsRecording(isActive);
                mSoundViewModel.setRecordProgress(0f);
                stopRecording();
            }
        });


        Log.d(DEBUG_TAG, "RecorderFragment() mRecordingThread :" + mRecordingThread);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate()");
        mRootDirectory = requireActivity().getFilesDir();
        if (savedInstanceState == null) {
            Log.d(DEBUG_TAG, "onCreate() - about to call startAudioRecordingSafe()");
            startAudioRecordingSafe();
        }
        isActive = false;
        mSoundViewModel = new ViewModelProvider(getParentFragment().getParentFragment()).get(SoundViewModel.class);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            printInfo();
//        }

    }

//    @RequiresApi(api = Build.VERSION_CODES.N)
//    void printInfo() {
//        requireActivity().getSystemService(Context.AUDIO_SERVICE);
//        AudioManager audioManager = (AudioManager) requireActivity().getSystemService(Context.AUDIO_SERVICE);
//        String unprocessed = audioManager.getProperty(AudioManager.PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED);
//        Log.e(DEBUG_TAG, "onCreate() PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED " + unprocessed);
//        PackageManager packageManager = requireActivity().getApplicationContext().getPackageManager();
//        boolean hasLowLatencyFeature =
//                packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_LOW_LATENCY);
//        boolean hasProFeature =
//                packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_PRO);
//        Log.e(DEBUG_TAG, "onCreate() FEATURE_AUDIO_LOW_LATENCY " + hasLowLatencyFeature);
//        Log.e(DEBUG_TAG, "onCreate() FEATURE_AUDIO_LOW_LATENCY " + hasProFeature);
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         // Inflate the layout for this fragment
        Log.d(DEBUG_TAG, "onCreateView()");
        View root = inflater.inflate(R.layout.fd_fragment_record, container, false);
        mRecordView = root.findViewById(R.id.record_view);
        mRecordButton = root.findViewById(R.id.record_button);
        mRecordButton.setActivated(false);
        Log.d(DEBUG_TAG, "onCreateView() recordView " + mRecordView + " recordButton " + mRecordButton);
        return root;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onViewCreated()");
        Log.d(DEBUG_TAG, "onViewCreated() - setting mRecordButton.setOnTouchListener()");

        mRecordButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    buttonClicked();
                    break;
                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
                    break;
            }
            return true;
        });

        Log.d(DEBUG_TAG, "onViewCreated() - setting mSoundViewModel.getIsRecording().observe()");
        mSoundViewModel.getIsRecording().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean started) {
                Log.d(DEBUG_TAG, " --> mSoundViewModel.getIsRecording()->onChanged(Boolean started) " + started);
                Log.d(DEBUG_TAG, " --> onChanged(Boolean started) - about to mRecordButton.setActivated(" +started+ ")");
                mRecordButton.setActivated(started);
                mRecordButton.invalidate();
            }
        });

        Log.d(DEBUG_TAG, "onViewCreated() - setting mSoundViewModel.getIsRecording().observe()");
        mSoundViewModel.getRecordProgress().observe(getViewLifecycleOwner(), new Observer<Float>() {
            @Override
            public void onChanged(Float aFloat) {
                mRecordButton.setIndicator(aFloat);
                mRecordButton.invalidate();
            }
        });
    }

    private void buttonClicked() {
        Log.d(DEBUG_TAG, "buttonClicked()");
        if (!isActive) {
            startRecording();
            mSoundViewModel.setIsRecording(isActive);
        }
        else {
            stopRecording();
            mSoundViewModel.setIsRecording(isActive);
            mSoundViewModel.setRecordProgress(0f);
        }
        isActive = !isActive;
    }

    public void startRecording() {
        Log.d(DEBUG_TAG, "startRecording()");
        Log.d(DEBUG_TAG, "startRecording() - getting blank file, call startRecording on " +
                "recThread and setEditMode false in viewmodel");
        mRecordingThread.setFile(mFile = getBlankFile());
        mRecordingThread.startRecording();
        mSoundViewModel.setEditMode(false);
        Log.d(DEBUG_TAG, "startRecording() - recording started");
    }

    public void stopRecording() {
        Log.d(DEBUG_TAG, "stopRecording()");
        if (mRecordingThread != null) {
            Log.d(DEBUG_TAG, "stopRecording() - mRecordingThread.stopRecording()");
            mRecordingThread.stopRecording();
        }
        Log.d(DEBUG_TAG, "stopRecording() - about to set transient file to " +
                "viewmodel and pass it to listener");
        mSoundViewModel.transientRecordingFile = mFile;
        if (mListener != null) {
            mListener.onEnded(mFile);
        }
    }

    // Fragment stop
    @Override
    public void onStop() {
        Log.d(DEBUG_TAG, "onStop()");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d(DEBUG_TAG, "onDestroy()");
        if (mRecordingThread != null) {
            Log.d(DEBUG_TAG, "onDestroy() - mRecordingThread != null, stopping thread");
            mRecordingThread.stopSafe();
        }
        Log.d(DEBUG_TAG, "onDestroy() - about to set mRecordingThread = null");
        mRecordingThread = null;
        super.onDestroy();
    }

    public File getTransientFile() {
        return mFile;
    }


    @NonNull
    private File getBlankFile() {
        Log.d(DEBUG_TAG, "getBlankFile()");
        File f = new File(mRootDirectory, "blank.wav");
        Log.d("OmText", "blank.wav exist? " + f.exists());
        if (f.exists()) f.delete();
        try {
            f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        f.renameTo(new File("blank.wav"));
        f = new File(mRootDirectory, "blank.wav");
        Log.d("OmText", "blank.wav exist? " + f.exists() + " name " + f.getName());
        return f;
    }

    // Those three last methods are for permission stuff
    private void startAudioRecordingSafe() {
        if (ContextCompat.checkSelfPermission(requireActivity(), android.Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.startSafe();
        } else {
            requestMicrophonePermission();
        }
    }

    private void requestMicrophonePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                android.Manifest.permission.RECORD_AUDIO)) {
            // Show dialog explaining why we need record audio
            Snackbar.make(mRecordView, "Microphone access is required in order to record audio",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK",
                    v -> ActivityCompat.requestPermissions(requireActivity(), new String[]{
                            android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO)).show();
        } else {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{
                    android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.stopSafe();
        }
    }

    @Override
    public void onStart() {
        Log.d(DEBUG_TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(DEBUG_TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(DEBUG_TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onDetach() {
        Log.d(DEBUG_TAG, "onDetach()");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(DEBUG_TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        Log.d(DEBUG_TAG, "onConfigurationChanged(@NonNull Configuration newConfig)");
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(DEBUG_TAG, "onAttach(@NonNull Context context)");
        super.onAttach(context);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d(DEBUG_TAG, "onSaveInstanceState(@NonNull Bundle outState)");
        super.onSaveInstanceState(outState);
    }
}
