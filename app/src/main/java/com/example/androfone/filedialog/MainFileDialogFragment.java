package com.example.androfone.filedialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.androfone.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

// There is another way to do this but for the time being we will leave it like this.
// Consider to refactor to Activity as Dialog; look at Android doc in AlertDialog
// which contain a little sideline which allow an activity to be displayed as a
// dialog floating over another activity with just one line of code using style.
// Here it is:
//Tip: If you want a custom dialog, you can instead display an Activity as a dialog
// instead of using the Dialog APIs. Simply create an activity and set its theme to
// Theme.Holo.Dialog in the <activity> manifest element:
//
//   <activity android:theme="@android:style/Theme.Holo.Dialog" >
//
//   That's it. The activity now displays in a dialog window instead of fullscreen.


/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     ItemListDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {FileDialogFragment.Listener}.</p>
 */
public class MainFileDialogFragment extends BottomSheetDialogFragment
        implements BottomNavigationLayout.BottomNavigationListener, DirectoryFragment.OnItemSelectedListener,
        View.OnClickListener, FileFragment.OnItemSelectedListener {

    private FragmentContainerView mContainer;

    private static final String DEBUG_TAG = "MainFileDialogFragment";

    private static final String TAB_KEY = "lastTab";
    private static final String DIR_KEY = "lastDirectory";
    private static final String DIR_ID_KEY = "lastDirectoryId";
    private static final String FROM_FRAG_KEY = "from_fragment";

    private static final String ARG_ENTRY_TAB = "entry_tab";
    private static final String ARG_FRAG_TAB = "from_frag";

    private final String[] mDefaultTitle = {"Sound bank", "My sounds", "My favorites"};
    BottomNavigationLayout mNavigationLayout;
    private TextView mHeaderTop;
    private AppCompatImageView mHeaderChild;
    private boolean isHeaderTop;

    private TextView mHeaderTitle;
    private TextView mHeaderEdit;
    private boolean isEditEnabled;
    private boolean isEditMode;
    private SoundViewModel soundViewModel;


    private String lastDirectorySelected = null;
    private long lastDirectorySelectedId;


    public static final int START_FROM_OPENFILE = 0;
    public static final int START_FROM_RECORD = 1;
    private int mStartParam;
    private String mFromTrackFragment;

    public interface OnFragmentResultListener {
        void onResult(SoundItem item, String tag);
    }
    private OnFragmentResultListener mFragmentResultListener;

//    public void setOnFragmentResultListener(OnFragmentResultListener listener) {
//        this.mFragmentResultListener = listener;
//    }


    public static MainFileDialogFragment newInstance(int startParam, String frag) {
        final MainFileDialogFragment fragment = new MainFileDialogFragment();
        final Bundle args = new Bundle();
        args.putInt(ARG_ENTRY_TAB, startParam);
        args.putString(ARG_FRAG_TAB, frag);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this is where we get transparent background for BottomSheetDialog
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.MyBottomSheetDialogTheme);

        if (savedInstanceState != null) {
            mStartParam = savedInstanceState.getInt(TAB_KEY, 0);
            lastDirectorySelected = savedInstanceState.getString(DIR_KEY, null);
            lastDirectorySelectedId = savedInstanceState.getLong(DIR_ID_KEY);
            mFromTrackFragment = savedInstanceState.getString(FROM_FRAG_KEY);
        } else { // First time
            // arguments set the entry point of the dialog to file or record tab
            if (getArguments() != null) {
                mStartParam = getArguments().getInt(ARG_ENTRY_TAB);
                mFromTrackFragment = getArguments().getString(ARG_FRAG_TAB);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mStartParam = mNavigationLayout.getSelected();
        outState.putInt(TAB_KEY, mStartParam);
        outState.putString(DIR_KEY, lastDirectorySelected);
        outState.putLong(DIR_ID_KEY, lastDirectorySelectedId);
        outState.putString(FROM_FRAG_KEY, mFromTrackFragment);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fd_fragment_main_file_dialog, container, false);

        soundViewModel = new ViewModelProvider(this).get(SoundViewModel.class);

        soundViewModel.getLiveInternalSoundsList().observe(this, soundItems -> {
            // This is only to refresh sound list in dir if favorite is swiped in this
            // current layout configuration.
            Log.d(DEBUG_TAG, "lastDirectorySelectedId: " + lastDirectorySelectedId);
            soundViewModel.setDirectoryId(lastDirectorySelectedId);
        });
        soundViewModel.getEditEnable().observe(getViewLifecycleOwner(), aBoolean -> setEditEnabled(aBoolean));
        soundViewModel.getEditMode().observe(getViewLifecycleOwner(), aBoolean -> setEditMode(aBoolean));
        return contentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        CardView root = view.findViewById(R.id.root_card_view);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm); // Find newer way to get metrics
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        int sideMargins = isTablet ? width / 8 : 0;
        int heightMargins = height / 6;
        int bottomMargin = isTablet ? heightMargins : 0;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(sideMargins, heightMargins + (heightMargins / 2), sideMargins, bottomMargin);
        root.setLayoutParams(params);

        Dialog dialog = getDialog();

        if (dialog instanceof BottomSheetDialog) {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            BottomSheetBehavior<?> behavior = d.getBehavior();
            behavior.setHideable(false);
            // setting peek height bigger than top will prevent from moving
            behavior.setPeekHeight(height);
//            behavior.setPeekHeight(2440); // replace magic number
        }

        mNavigationLayout = view.findViewById(R.id.navigation_view);
        mNavigationLayout.setBottomNavigationListener(this);
        mContainer = view.findViewById(R.id.fragment_container);
        FrameLayout mHeaderUp = view.findViewById(R.id.header_up_nav);
        mHeaderUp.setOnClickListener(this);
        mHeaderTop = view.findViewById(R.id.header_top);
        mHeaderChild = view.findViewById(R.id.header_child);
        mHeaderTitle = view.findViewById(R.id.header_title);
        mHeaderEdit = view.findViewById(R.id.header_edit);
        mHeaderEdit.setOnClickListener(this);
        mNavigationLayout.setSelected(mStartParam);
        soundViewModel.setEditEnable(mStartParam != START_FROM_OPENFILE);
        boolean isTop = true;
        if (lastDirectorySelected != null) isTop = false;
        setHeaderTop(isTop);

    }

    private void setEditEnabled(boolean isEnabled) {
        isEditEnabled = isEnabled;
        mHeaderEdit.setEnabled(isEnabled);
    }

    private void setEditMode(boolean isActive) {
        isEditMode = isActive;
        mHeaderEdit.setText(isEditMode ? "Done" : "Edit");
    }

    @Override
    public void onTabSelected(int position) {
        switch (position) {
            case 0:
                Log.d(DEBUG_TAG, "lastDirectorySelected: " + lastDirectorySelected);
                if (lastDirectorySelected != null) {
                    String tag = "file";
                    FileFragment fileFragment =
                            (FileFragment) getChildFragmentManager().findFragmentByTag(tag);
                    if(fileFragment == null) {
                        fileFragment = new FileFragment();
                    }
                    fileFragment.setOnItemSelectedListener(this);
                    insertNestedFragment(fileFragment, tag);
                    mHeaderTitle.setText(lastDirectorySelected);
                    mHeaderEdit.setVisibility(View.INVISIBLE);
                    setHeaderTop(false);
                } else {
                    String tag = "dir";
                    DirectoryFragment directoryFragment =
                            (DirectoryFragment) getChildFragmentManager().findFragmentByTag(tag);
                    if(directoryFragment == null) {
                        directoryFragment = new DirectoryFragment();
                    }
                    insertNestedFragment(directoryFragment, tag);
                    mHeaderTitle.setText(mDefaultTitle[0]);
                    mHeaderEdit.setVisibility(View.INVISIBLE);
                    setHeaderTop(true);
                }
                break;
            case 1:
                String tag = "user";
                UserFragment userFragment =
                        (UserFragment) getChildFragmentManager().findFragmentByTag(tag);
                if(userFragment == null) {
                    userFragment = new UserFragment();
                    userFragment.setRetainInstance(true);
                }
                soundViewModel.setEditMode(false); // alway reset edit mode on page change
                userFragment.setOnItemSelectedListener(this);
                insertNestedFragment(userFragment, tag);
                mHeaderTitle.setText(mDefaultTitle[1]);
                isEditEnabled = isEditMode;
                mHeaderEdit.setVisibility(View.VISIBLE);
                mHeaderEdit.setEnabled(isEditEnabled);
                setHeaderTop(true);
                break;
            case 2:
                tag = "fav";
                FavoritesFragment favoritesFragment =
                        (FavoritesFragment)getChildFragmentManager().findFragmentByTag(tag);
                if(favoritesFragment == null) {
                    favoritesFragment = new FavoritesFragment();
                    favoritesFragment.setRetainInstance(true);
                }
                soundViewModel.setEditMode(false); // alway reset edit mode on page change
                favoritesFragment.setOnItemSelectedListener(this);
                insertNestedFragment(favoritesFragment, tag);
                mHeaderTitle.setText(mDefaultTitle[2]);
                isEditEnabled = isEditMode;
                mHeaderEdit.setVisibility(View.VISIBLE);
                mHeaderEdit.setEnabled(isEditEnabled);
                setHeaderTop(true);
        }
        Log.d(DEBUG_TAG, "item clicked pos: " + position);
    }

    // Embeds the child fragment dynamically
    private void insertNestedFragment(Fragment frag, String tag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(mContainer.getId(), frag, tag).commit();
    }

    // handler for navigation from directory -> file fragment
    // override from DirectoryFragment
    @Override
    public void onItemSelected(String name, long id) {
        Log.d(DEBUG_TAG, "item received: " + name);
        lastDirectorySelected = name;
        soundViewModel.setDirectoryId(id);
        lastDirectorySelectedId = id;

        String tag = "file";
        FileFragment frag = (FileFragment)getChildFragmentManager().findFragmentByTag(tag);
        if(frag == null) {
            frag = new FileFragment();
        }
        frag.setOnItemSelectedListener(this);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.fd_slide_in_right, R.animator.fd_slide_out_left);
        transaction.replace(mContainer.getId(), frag, tag).commit();
        mHeaderTitle.setText(name);
        setHeaderTop(false);
    }

    // Handle header click
    // Handle edit click
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.header_up_nav) {
            handleNavUp();
        } else if (id == R.id.header_edit) {
            if (isEditEnabled) {
                // toggle edit mode
                isEditMode = !isEditMode;
                soundViewModel.setEditMode(isEditMode);
            }
        }
    }

    // handler for navigation from file -> directory or back to activity
    private void handleNavUp() {
        if (isHeaderTop) {
            dismiss(); // Get out of here
        } else {
            Fragment frag = new DirectoryFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.animator.fd_slide_in_left, R.animator.fd_slide_out_right)
                    .replace(mContainer.getId(), frag)
                    .commit();
            mHeaderTitle.setText(mDefaultTitle[0]);
            setHeaderTop(true);
            lastDirectorySelected = null;
        }
    }

    // switch button display for top(directory) or child(file)
    private void setHeaderTop(boolean isTop) {
        isHeaderTop = isTop;
        if (isHeaderTop) {
            mHeaderChild.setVisibility(View.INVISIBLE);
            mHeaderTop.setVisibility(View.VISIBLE);
        } else {
            mHeaderChild.setVisibility(View.VISIBLE);
            mHeaderTop.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onItemSelected(SoundItem item, int position) {
        String tag = mFromTrackFragment;
        Log.d(DEBUG_TAG, "!!!!!!!!! onItemSelected tag: " + tag);
        if (mFragmentResultListener != null) {
            Log.d(DEBUG_TAG, "!!!!!!!!! mFragmentResultListener != null: " + item.getName());
            mFragmentResultListener.onResult(item, tag);
            // Close the dialog
            dismiss();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Verify that the parent fragment implements the callback interface
        Activity activity = (Activity)context;
        try {
            // Instantiate the OnOKButtonClickListener so we can send events to the host
            mFragmentResultListener = (OnFragmentResultListener) activity;
        } catch (ClassCastException e) {
            // The fragment doesn't implement the interface, throw exception
            throw new ClassCastException(getParentFragment().toString()
                    + " must implement OnFragmentResultListener");
        }
    }

}
