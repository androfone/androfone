package com.example.androfone.filedialog;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.io.File;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/////// Investigate this:
// warning: Schema export directory is not provided to the annotation processor so we cannot export
// the schema. You can either provide `room.schemaLocation` annotation processor argument OR set
// exportSchema to false.
//public abstract class SoundDatabase extends RoomDatabase {

// Best practice:
//In the build.gradle file for your app module, add this to the defaultConfig
// section (under the android section). This will write out the schema to a schemas
// subfolder of your project folder.
//
//        javaCompileOptions {
//        annotationProcessorOptions {
//        arguments += ["room.schemaLocation": "$projectDir/schemas".toString()]
//        }
//        }
//

// Or for the time being
//@Database(entities = { YourEntity.class }, version = 1, exportSchema = false)
//public abstract class AppDatabase extends RoomDatabase {
//    //...
//}

//@Database(entities = {SoundItem.class, DirectoryItem.class}, version = 1)
@Database(entities = {SoundItem.class, DirectoryItem.class}, version = 1, exportSchema = false)
@TypeConverters(SoundDatabase.Converters.class)
public abstract class SoundDatabase extends RoomDatabase {

    private static final String DEBUG_TAG = "SoundDatabase";

    private static SoundDatabase instance;

    public abstract SoundItemDao soundItemDao();
    public abstract DirectoryItemDao directoryItemDao();
    private static DirectoryItem userDirectory;

    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    static final Handler handler = new Handler(Looper.getMainLooper());
    private static SoundRepository.SetUserDirectoryCB setUserDirectoryCB;


    public static synchronized SoundDatabase getInstance(Context context, SoundRepository.SetUserDirectoryCB cb) {
        if (instance == null) {
            Log.d(DEBUG_TAG," instance == null");
            setUserDirectoryCB = cb;
            File base = context.getFilesDir();
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    SoundDatabase.class, "database")
                    .fallbackToDestructiveMigration()
//                    .addCallback(new RoomDatabase.Callback() {
//                        @Override
//                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
//                            super.onCreate(db);
//
//                            Log.d(DEBUG_TAG," inside callback - calling PopulateSoundDBAsyncTask");
//                            new PopulateSoundDBAsyncTask(instance).execute(base);
//                        }
//                    })
                    .build();
            Log.d(DEBUG_TAG,"calling PopulateSoundDB");
            new PopulateSoundDB(instance, base).populate();
        }
        Log.d(DEBUG_TAG," instance still null?: " + (instance == null));
        return instance;
    }

//    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
//        @Override
//        public void onCreate(@NonNull SupportSQLiteDatabase db) {
//            super.onCreate(db);
//
//            File base = mContext.getFilesDir();
//            Log.d(DEBUG_TAG," inside callback - calling PopulateSoundDBAsyncTask");
//
//            new PopulateSoundDBAsyncTask(instance).execute(base);
//        }
//    };

    private static class PopulateSoundDB {
        private final DirectoryItemDao directoryItemDao;
        private final SoundItemDao soundItemDao;
        private final File base;

        PopulateSoundDB(SoundDatabase db, File root) {
            directoryItemDao = db.directoryItemDao();
            soundItemDao = db.soundItemDao();
            base = root;
        }

        void populate() {
            databaseWriteExecutor.execute(() -> {
                // clear previous database
                directoryItemDao.deleteAll();
                soundItemDao.deleteAll();
                // construct new database
                // Recurse through the sound bank files and insert dir and sound item into the database
                String rootDir = "sound bank"; // Perhaps those two lines could come from some resources
                String userDir = "user sounds";
                File root = base;  // == getFilesDir()
                String path = root.getPath() + File.separator + rootDir;
                File bank = new File(path);
                File[] directoryList = bank.listFiles();
                int count = 0;
                if (directoryList != null) {
                    for (File dir : directoryList) {
                        DirectoryItem d = new DirectoryItem(dir.getName(), root.toURI().relativize(dir.toURI()), count++, true);
                        directoryItemDao.insert(d); // Now the item have an id
                        // We need to do this to get the id set by the database
                        DirectoryItem ditem = directoryItemDao.getDirectory(dir.getName());
                        long id = ditem.getId();
                        File[] files = dir.listFiles();
                        if (files != null) {
                            for (File f : files) {
                                String name = f.getName().substring(0, f.getName().length()-4);
                                SoundItem s = new SoundItem(name, root.toURI().relativize(f.toURI()),
                                        id, false, 0);
                                soundItemDao.insert(s);
                            }
                        }
                    }
                }

                // Do the same for the user directory
                String userpath = root.getPath() + File.separator + userDir;
                File user = new File(userpath);
                directoryItemDao.insert(new DirectoryItem(user.getName(), root.toURI().relativize(user.toURI()), 100, false));
                DirectoryItem udir = directoryItemDao.getDirectory(user.getName());
                long id = udir.getId();
                File[] files = user.listFiles();
                if (files != null) {
                    for (File f : files) {
                        String name = f.getName().substring(0, f.getName().length()-4);
                        SoundItem s = new SoundItem(name, root.toURI().relativize(f.toURI()),
                                id, false, 1);
                        soundItemDao.insert(s);
                    }
                }
                userDirectory = udir;
                Log.d(DEBUG_TAG,"SoundDatabase::PopulateSoundDB user directory name: " + udir.getName() + " id: " + udir.getId());
                // Post back userDirectory to SoundRepository through callback
                handler.post(() -> setUserDirectoryCB.setUserDirectory(userDirectory));
                Log.d(DEBUG_TAG,"SoundDatabase::PopulateSoundDB finished");
            });
        }

    }

    // Utility class
    static class Converters {

        @TypeConverter
        public URI stringToUri(String uri) {
            return URI.create(uri);
        }

        @TypeConverter
        public String uriToString(URI uri) {
            return uri.toString();
        }

    }
}
