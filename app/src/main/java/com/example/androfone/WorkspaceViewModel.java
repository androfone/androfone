package com.example.androfone;

import android.app.Application;
import android.graphics.Point;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WorkspaceViewModel extends ViewModel {

    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();
    private MutableLiveData<Boolean> mEditMode = new MutableLiveData<>();
//    private MutableLiveData<Boolean> mIsInteracting = new MutableLiveData<>();
    private MutableLiveData<Float> mFilterCutoff = new MutableLiveData<>();
    private MutableLiveData<Float> mFilterResonance = new MutableLiveData<>();
    private MutableLiveData<Point> mFilterCursorPosition = new MutableLiveData<>();


    public WorkspaceViewModel() {
        super();
    }

    public void setEditMode(boolean isEditMode) {
        Log.e("mViewModel", "isEditMode: " + isEditMode);
        mEditMode.setValue(isEditMode);
    }

    public LiveData<Boolean> getEditMode() {
        return  mEditMode;
    }

//    public void setIsInteracting(boolean isInteracting) { mIsInteracting.setValue(isInteracting); }
//
//    public LiveData<Boolean> getIsInteracting() { return mIsInteracting; }

    public void setFilterCutoff(float fc) { mFilterCutoff.setValue(fc); }

    public LiveData<Float> getFilterCutoff() { return mFilterCutoff; }

    public void setFilterResonance(float fq) {
        mFilterResonance.setValue(fq);
    }

    public LiveData<Float> getFilterResonance() { return mFilterResonance; }

    public void setFilterCursorPosition(Point p) { mFilterCursorPosition.setValue(p); }

    public LiveData<Point> getFilterCursorPosition() { return mFilterCursorPosition; }

}
