package com.example.androfone;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.example.androfone.filedialog.SoundItem;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class WorkspaceFragment extends Fragment implements View.OnDragListener, View.OnTouchListener {

    // When requested, this adapter returns a PageFragment,
    // representing one page of the workspace.
    private PageFragmentAdapter mPageFragmentAdapter;
    private ViewPager2 mViewPager;
    private int mNumPages;

    private static final String TAG_LIST_KEY = "tagList";
    private static final String CUR_TAB_KEY = "curTab";
    private static final String CUR_POS_KEY = "curPos";
    private static final String CUR_FRAG_KEY = "curFrag";

    private ArrayList<Fragment> mFragmentList = new ArrayList<>(mNumPages);
    private ArrayList<String> mFragmentTagList = new ArrayList<>(mNumPages);
    private FragmentManager mChildFragmentManager;

    private TabLayout mTabLayout;
    private int mCurTab;
    private int mCurPosition = -1;

    private boolean isEditMode = false;
    String debugTag = "WorkspaceFragment";

    private WorkspaceViewModel mViewModel;

    private SoundItem mCurSound = null;

    // drag drop related variables
    private RelativeLayout mCollisionLayer;
    private RelativeLayout mToolLayer;
    ArrayList<Rect> mRectangles = new ArrayList<>();
    private View mDragView = null;
    private boolean isExited;
    public Handler mHandler = new Handler();
    boolean isInPageChangeZone = false;
    boolean isInLeftPageChangeZone = false;
    boolean isInRightPageChangeZone = false;
    private int pageChangeStepValue;
    private static final int PageUp = 1;
    private static final int PageDown = -1;
    private int mGridSize;
    private float mHorizontalGridAdjust;
    float mLastXStep;
    float mLastYStep;
    private Rect mCollisionRect;
    private boolean mIsDropEnable = true;
    private int mWorkspaceWidth;
    private Rect mWorkspaceRect;
    private int mTriggerZoneWidth;
    private int mRedColor;
    private int mGreenColor;

    private final Point mClickPointOffset = new Point();
    public void setClickPointOffset(int x, int y) {
        mClickPointOffset.x = x;
        mClickPointOffset.y = y;
        Log.e(debugTag+getTag(), "setClickPointOffset(int x, int y) x: " + x + " y: " + y);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mChildFragmentManager = getChildFragmentManager();
        mNumPages = getResources().getInteger(R.integer.num_of_tabs);

        if (savedInstanceState == null) {
            Log.e(debugTag + getTag(), "onCreateView() savedInstanceState == null fragmentList pre: " + mFragmentList);
            if (mFragmentList.isEmpty()) {
                Log.e(debugTag + getTag(), "onCreateView(): fragmentList empty - initialize fragmentList to null");
                for (int i = 0; i < mNumPages; i++) {
                    mFragmentList.add(null);
                }
                Log.e(debugTag + getTag(), "onCreateView() savedInstanceState == null fragmentList post: " + mFragmentList);
            } else {
                Log.e(debugTag + getTag(), "onCreateView() fragmentList not empty - reconstruct fTagList from fragmentList");
                Log.e(debugTag + getTag(), "onCreateView() savedInstanceState == null fTagList pre: " + mFragmentTagList +" fragmentList: " + mFragmentList);
                mFragmentTagList.clear();
                for( Fragment f : mFragmentList) {
                    if (f == null) mFragmentTagList.add(null);
                    else mFragmentTagList.add(f.getTag());
                }
                Log.e(debugTag + getTag(), "onCreateView() savedInstanceState == null fTagList post: " + mFragmentTagList +" fragmentList: " + mFragmentList);
            }
        }

        if (savedInstanceState != null) {
            Log.e(debugTag + getTag(), "onCreateView(): savedInstanceState != null");
            Log.e(debugTag + getTag(), "onCreateView(): reconstructing fragmentList from savedInstance fTagList");
            mFragmentList.clear();
            mFragmentTagList = savedInstanceState.getStringArrayList(TAG_LIST_KEY);
            Log.e(debugTag + getTag(), "onCreateView(): fTagList: " + mFragmentTagList);
            for (String str : mFragmentTagList) {
                if (str == null) mFragmentList.add(null);
                else mFragmentList.add(mChildFragmentManager.findFragmentByTag(str));
            }
            Log.e(debugTag + getTag(), "onCreateView(): fragmentList: " + mFragmentList);
        }

        Log.e(debugTag + getTag(), "onCreateView() this: " + this);

        return inflater.inflate(R.layout.workspace_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mPageFragmentAdapter = new PageFragmentAdapter(getChildFragmentManager(), getLifecycle());
        mViewPager = view.findViewById(R.id.pager);

        // DO NOT TAKE INTO ACCOUNT COMMENTS BELOW
        // COMMENTS BELOW HAVE BEEN DEDUCE FROM THE DOCUMENTATION BUT IT IS NOT ACCURATE
        // THE ONLY SETTING THAT HAVE PROVE TO WORK YET IS OFFSCREENPAGELIMIT = 1
        // FOR BOTH 3 AND 5 PAGES

        // !!!! CRUCIAL SETTING !!!
        // for 3 tabs, setOffscreenPageLimit shoult NOT be set - left to default setting
        // for 5 tabs, it should be set to 1
        // for 7 tabs, it should be set to 2 (if we ever need to have more pages)
//        if (mNumPages == 5)
            mViewPager.setOffscreenPageLimit(1);
        Log.e(debugTag + getTag(), "onViewCreated offscreenPageLimit: " + mViewPager.getOffscreenPageLimit());
        mViewPager.setAdapter(mPageFragmentAdapter);
        if (mCurPosition == -1) mCurPosition = mPageFragmentAdapter.getCenterPage();
        if (savedInstanceState != null) mCurPosition = savedInstanceState.getInt(CUR_POS_KEY,
                mPageFragmentAdapter.getCenterPage());
        mViewPager.setCurrentItem(mCurPosition, false);
        // Set drag listener
        mViewPager.setOnDragListener(this);

        if (savedInstanceState != null) mCurTab = savedInstanceState.getInt(CUR_TAB_KEY, 0);
//        else mCurTab = mViewPager.getCurrentItem() % mNumPages; // modulo statement
        else mCurTab = mViewPager.getCurrentItem(); // non modulo statement

        // TODO: Need to adjust (reduce) spacing if mNumPages = 5
        mTabLayout = view.findViewById(R.id.workspace_tab_layout);
        for (int i = 0; i < mNumPages; i++) {
            mTabLayout.addTab(mTabLayout.newTab());
        }

        mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.e(debugTag+getTag(), " * * onPageSelected(int position) position: " + position);
                Log.e(debugTag+getTag(), " * * onPageSelected(int position) mCurPosition: " + mCurPosition);

                mCurPosition = position;
//                if (position%mNumPages != mCurTab) mCurTab = position%mNumPages; // modulo statement
                if (position != mCurTab) mCurTab = position; // non modulo statement

                mTabLayout.selectTab(mTabLayout.getTabAt(mCurTab), true);
                if (isEditMode) {
                    Fragment frag = mPageFragmentAdapter.getFragment(position);
                    mCollisionLayer = ((PageFragment)frag).getCollisionLayer();
                    mToolLayer = ((PageFragment)frag).getToolLayer();
                    // set long click listener for drag start to all the tools in the page.
                    setListeners(true);
                    // dragView will be null outside a drag event,
                    // collect rect only when page change while dragging
                    if (mToolLayer != null && mDragView != null) {
                        collectRectangles();
                    }
                }
            }
        });

        mTabLayout.getTabAt(mCurTab).select();
        // For the time being, do not select the page from the tab, only use it as indicator.
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() != mCurTab) {
                    mCurTab = tab.getPosition();
//                    // modulo block
//                    if (mCurPosition%mNumPages != mCurTab) {
//                        int lastPos = mViewPager.getCurrentItem()%mNumPages - mCurTab;
//                        mViewPager.setCurrentItem(mViewPager.getCurrentItem()-lastPos, true);
//                    }
                    // non modulo block
                    if (mCurPosition != mCurTab) {
                        mViewPager.setCurrentItem(mCurTab, true);
                    }
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        mViewModel = new ViewModelProvider(this).get(WorkspaceViewModel.class);
        mViewModel.setEditMode(false);


//        Log.e(debugTag+getTag(), " onViewCreated() initializing drag variables");
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int unit = getResources().getInteger(R.integer.unit);
//        Log.e(debugTag+getTag(), " onViewCreated() unit: " + unit);
        mTriggerZoneWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, displayMetrics);
//        Log.e(debugTag+getTag(), " onViewCreated() mTriggerZoneWidth: " + mTriggerZoneWidth);
        int w = displayMetrics.widthPixels;
        Log.e(debugTag+getTag(), " onViewCreated() WIDTH: " + w);
        mRedColor = getResources().getColor(R.color.collision_shadow_collide_color);
        mGreenColor = getResources().getColor(R.color.collision_shadow_open_color);

    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        Log.e(debugTag+getTag(), " * ! * ! * ! * ! * ! * ! * ! * fragment setEditMode() isEditMode: " + isEditMode);
        Fragment frag = mPageFragmentAdapter.getFragment(mCurPosition);
        mCollisionLayer = ((PageFragment)frag).getCollisionLayer();
        mToolLayer = ((PageFragment)frag).getToolLayer();
        setOnDragListener(editMode);
        setListeners(editMode);
        mViewModel.setEditMode(editMode);
    }

    // In edit mode, set "this" as the drag listener for the viewpager, otherwise remove - set to null
    private void setOnDragListener(boolean b) {
        View.OnDragListener listener = b ? this : null;
        Log.e(debugTag+getTag(), " * * * * * * * * setOnDragListener() listener: " + listener);
        mViewPager.setOnDragListener(listener);
    }

    // In edit mode, set "this" as the onTouch listener for all the tool views in mToolLayer,
    // otherwise remove - set all to null. Those listeners are used to initialize and start
    // drag events for tools in the workspace while in edit mode.
    private void setListeners(boolean b) {
        View.OnTouchListener listener = b ? this : null;
        if (mToolLayer != null) {
            for (int i=0; i<mToolLayer.getChildCount(); i++) {
                View child = mToolLayer.getChildAt(i);
                child.setOnTouchListener(listener);
                Log.e(debugTag+getTag(), " * * * * * * * * setListeners() child: " + child);
                Log.e(debugTag+getTag(), " * * * * * * * * setListeners() listener: " + listener);
            }
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
            Log.e(debugTag + getTag(), "fragment.onViewStateRestored(): fTagList: " + mFragmentTagList);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mFragmentTagList.clear();
        for( Fragment f : mFragmentList) {
            if (f == null) mFragmentTagList.add(null);
            else {
                mFragmentTagList.add(f.getTag());

            }
        }
        Log.e(debugTag + getTag(), "onSaveInstanceState(): fTagList: " + mFragmentTagList);
        outState.putStringArrayList(TAG_LIST_KEY, mFragmentTagList);
        Log.e(debugTag + getTag(), "onSaveInstanceState(): CHECK bundle post: " + outState.getStringArrayList(TAG_LIST_KEY));
        Log.e(debugTag + getTag(), "onSaveInstanceState(): mViewPager: " + mViewPager);

        outState.putInt(CUR_TAB_KEY, mCurTab);
        outState.putInt(CUR_POS_KEY, mCurPosition);
        outState.putString(CUR_FRAG_KEY, getTag());
    }


    @Override
    public void onStop() {
        for( Fragment f : mFragmentList) {
            if (f != null) {
                ((PageFragment)f).saveLocalState();
            }
        }
        mCurPosition = mViewPager.getCurrentItem();
        super.onStop();
        Log.e(debugTag + getTag(), "fragment.onStop(): fTagList: " + mFragmentTagList + " fragmentList: " + mFragmentList);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(debugTag + getTag(), "fragment.onStart(): fTagList: " + mFragmentTagList + " fragmentList: " + mFragmentList);
    }
    @Nullable
    public void setCurSoundItem(@Nullable SoundItem item) {
        mCurSound = item;
    }

    public SoundItem getCurSoundItem() {
        return mCurSound;
    }

    private boolean isViewInBounds(Rect r) {

//        Log.e(debugTag + getTag(), "isViewInBounds(Rect r) mWorkspaceRect: " + mWorkspaceRect);
//        Log.e(debugTag + getTag(), "isViewInBounds(Rect r) r: " + r);
        if (!mWorkspaceRect.contains(r)) return true; // Early return if crossing outside view pager
        for (Rect rect : mRectangles) {
//            Log.e(debugTag + getTag(), "isViewInBounds(Rect r): mRectangles: " + mRectangles);
//            Log.e(debugTag + getTag(), "isViewInBounds(Rect r): rect: " + rect + " r: " + r);
            if (r.intersect(rect)) return true; // early return on first intersect
        }
        return false;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        mDragView = (View) event.getLocalState();
        int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                Log.e(debugTag + getTag(), "DragEvent.ACTION_DRAG_STARTED");

                mCollisionRect.offsetTo((int)x - mClickPointOffset.x, (int)y - mClickPointOffset.y);

                // Determines if this View can accept the dragged data
                return event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            case DragEvent.ACTION_DRAG_ENTERED:
                isExited = false;
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
//                Log.e(debugTag + getTag(), "DragEvent.ACTION_DRAG_LOCATION");

                float dx = x - mClickPointOffset.x;
                float dy = y - mClickPointOffset.y;
                // determine how many "step" we are in each grid dimensions
                float xStep = Math.round((dx- mHorizontalGridAdjust)/ mGridSize);
                float yStep = Math.round((dy)/ mGridSize);

//                Log.e(debugTag + getTag(), "onDrag x, y: " + x + " " + y);
                // Set collision rect quantized coord only if either xStep or yStep have changed
                if ((xStep != mLastXStep) || (yStep != mLastYStep)) {
                    mLastXStep = xStep;
                    mLastYStep = yStep;
                    // resulting quantized coordinates values
                    xStep = (xStep* mGridSize) + mHorizontalGridAdjust;
                    yStep = yStep* mGridSize;
//                  Log.e("Grid", "xstep_pixel : " + xStep );

                    mCollisionRect.offsetTo((int)xStep, (int)yStep);
                    mCollisionLayer.setClipBounds(mCollisionRect);
//                    Log.e(debugTag, "mCollisionRect: " + mCollisionRect );

                    // BIZARRE isViewInBounds seems to have side effects on collisionRect
                    // so we pass a copy
                    Rect testR = new Rect(mCollisionRect);
                    if(isViewInBounds(testR)) {
//                    Log.e(debugTag, "!!! View is Over !!!" );
                        mCollisionLayer.setBackgroundColor(mRedColor);
                        mCollisionLayer.invalidate();
                        mIsDropEnable = false;
                    } else {
//                    Log.e(debugTag, "!!! View clear !!!" );
                        mCollisionLayer.setBackgroundColor(mGreenColor);
                        mCollisionLayer.invalidate();
                        mIsDropEnable = true;
                    }
                    mCollisionLayer.invalidate();
                }


                // initiate page change if drag position is in page change zone
                if ((int) x < mTriggerZoneWidth && !isInLeftPageChangeZone) {
                    Log.e("DragDrop", "!!! x < mTriggerZoneWidth && !isInPageChangeZone !!!");
                    Log.e("DragDrop", "!!! x: " + x + " mTriggerZoneWidth: " + mTriggerZoneWidth + " isInPageChangeZone: " + isInPageChangeZone);
                    isInPageChangeZone = isInLeftPageChangeZone = true;
                    pageChangeStepValue = PageDown;
                    runnable.run();

                }
                if ((int) x > mWorkspaceWidth - mTriggerZoneWidth && !isInRightPageChangeZone) {
                    Log.e("DragDrop", "!!! x > workspaceWidth-mTriggerZoneWidth && !isInRightPageChangeZone !!!");
                    Log.e("DragDrop", "!!! x: " + x + " mTriggerZoneWidth: " + mTriggerZoneWidth + " isInPageChangeZone: " + isInPageChangeZone);
                    isInPageChangeZone = isInRightPageChangeZone = true;
                    pageChangeStepValue = PageUp;
                    runnable.run();
                }

                if (((int)x > mTriggerZoneWidth && (int)x < mWorkspaceWidth - mTriggerZoneWidth) && isInPageChangeZone) {
                    Log.e("DragDrop", "!!! not in pageChangeZone !!!" );
                    mHandler.removeCallbacks(runnable);
                    isInPageChangeZone = isInLeftPageChangeZone = isInRightPageChangeZone = false;
                    // Clear background on all other page to prevent artifacts
                    for (int i=0; i<mNumPages; i++) {
//                        if (i == mCurPosition%mNumPages) continue; // modulo statement
                        if (i == mCurPosition) continue; // non modulo statement
                        PageFragment f = (PageFragment) mPageFragmentAdapter.getFragment(mCurPosition);
                        if (f.getCollisionLayer().getBackground() != null)
                            f.getCollisionLayer().setBackground(null);
                        f.getCollisionLayer().invalidate();
                    }
                }
                mCollisionLayer.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                Log.e(debugTag + getTag(), "DragEvent.ACTION_DRAG_EXITED");
                isExited = true;
                // Stop ongoing runnable
                mHandler.removeCallbacks(runnable);
                // Turns off any color tints
                mCollisionLayer.setBackground(null);
                mCollisionLayer.invalidate();

                // Invalidate the view to force a redraw in the new tint
                getView().invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                Log.e(debugTag + getTag(), "DragEvent.ACTION_DROP");
                // Stop ongoing runnable
                mHandler.removeCallbacks(runnable);
                // Turns off any color tinting for all fragments
                for (int i=0; i<mNumPages; i++) {
//                    if (i == mViewPager.getCurrentItem()%mNumPages) continue; // modulo statement
                    if (i == mViewPager.getCurrentItem()) continue; // non modulo statement
                    if (mCollisionLayer.getBackground() != null)
                        mCollisionLayer.setBackground(null);
                    mCollisionLayer.invalidate(); // ????????????? is this the right place?
                }
                if (mIsDropEnable) {
                    ViewGroup owner = (ViewGroup) mDragView.getParent();
                    //cast the view into RelativeLayout as our drag acceptable layout is RelativeLayout
                    int sizeh = (int) mDragView.getWidth();
                    int sizev = (int) mDragView.getHeight();
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(sizeh, sizev);

                    Rect r = mCollisionLayer.getClipBounds();
                    p.leftMargin = r.left;
                    p.topMargin = r.top;

                    if (event.getClipDescription().getLabel().equals("MENU")) {
                        Log.e("DragDrop Example", "TOOL FROM MENU");

                        View tool = ToolSet.getInstanceByClassName(getContext(),
                                mViewModel, mDragView.getClass().getSimpleName());
                        tool.setLayoutParams(p);
                        tool.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE
                        tool.setOnTouchListener(this);  // TODO: check for override Tool.performClick
                        mToolLayer.addView(tool); // Add a new tool View;

                    } else {
                        // Dragged view is from the same page
                        if (owner == mToolLayer) { // only update view layout position
                            Log.e("DragDrop Example", "owner == container");
                            mDragView.setLayoutParams(p);
                            mDragView.setVisibility(View.VISIBLE);
                        }
                        // Dragged view is from another page
                        else if (owner != mToolLayer) { // remove the view from its old root
                            Log.e("DragDrop Example", "owner != container");
                            owner.removeView(mDragView);
                            mDragView.setLayoutParams(p);
                            mDragView.setVisibility(View.VISIBLE);
                            mToolLayer.addView(mDragView);
                        }
                    }
                    // Invalidates the view to force a redraw
                    mToolLayer.invalidate();
                    // Returns true. DragEvent.getResult() will return true.
                    return true;
                    // else the drop didn't work. DragEvent.getResult() will return false.
                } else {
                    // reset the view visibility
                    mDragView.setVisibility(View.VISIBLE);
                    return false;
                }

            case DragEvent.ACTION_DRAG_ENDED:
                Log.e(debugTag + getTag(), "DragEvent.ACTION_DRAG_ENDED");
                if(isExited) {
                    // Handle exited as a delete view
                    Log.e("DragDrop Example", "ACTION_DRAG_ENDED if(isExited)");
                    ViewGroup owner = (ViewGroup) mDragView.getParent();
                    if (owner != null && !owner.getClass().getSimpleName().equals("ToolSet")) {
                        owner.removeView(mDragView);
                    }
                    Log.e(debugTag + getTag(), "owner: " + owner.getClass().getSimpleName());


                }

                // Turns off any color tinting for all fragments
                // Invalidates the views to force a redraw
                for (int i=0; i<mNumPages; i++) {
//                    if (i == mCurPosition%mNumPages) continue; // modulo statement
                    if (i == mCurPosition) continue; // non modulo statement
//                    WorkspaceFragment f = (WorkspaceFragment) mPageFragmentAdapter.getItem(i);
                    PageFragment f = (PageFragment)mFragmentList.get(i);
                    if (f != null) {
                        if (f.mCollisionLayer != null) {
                            f.mCollisionLayer.setBackground(null);
                            f.mCollisionLayer.invalidate();
                        }
                    }
                }
                // Turns off any color tinting for this mCollisionLayer
                mCollisionLayer.setBackground(null);
                mCollisionLayer.setClipBounds(null);

                // BE AWARE OF THIS!!!!
                mCollisionRect = null;

                // reset dragView to null
                mDragView = null;
                mCollisionLayer.invalidate();
                return true;
            default:
                Log.e(debugTag + getTag(), "Unknown action type received by OnDragListener.");
                break;
        }
        return true;
    }

    private void initDragVariables() {
        // Initialize dragdrop variables
        Log.e(debugTag+getTag(), " initDragVariables() finish initializing drag variables");
        mWorkspaceWidth = mViewPager.getWidth();
        Log.e(debugTag, " initDragVariables() mWorkspaceWidth: " + mWorkspaceWidth);
        mHorizontalGridAdjust = GridView.getHorizontalGridAdjust();
//        Log.e(debugTag, " initDragVariables() mHorizontalGridAdjust: " + mHorizontalGridAdjust);
        mGridSize = GridView.getGridSize();
//        Log.e(debugTag, " initDragVariables() mGridSize: " + mGridSize);
        mWorkspaceRect = new Rect();
        mViewPager.getDrawingRect(mWorkspaceRect);
        Log.e(debugTag, " ** ! ** ! ** ! ** ! ** ! ** ! ** ! test workspaceRect: " + mWorkspaceRect);
        Fragment frag = mPageFragmentAdapter.getFragment(mCurPosition);
//        Log.e(debugTag, " ** ! ** ! ** ! ** ! ** ! ** ! ** ! test mCurPosition: " + mCurPosition);
        Log.e(debugTag+getTag(), " ** ! ** ! ** ! ** ! ** ! ** ! ** ! cur frag: " + frag);
        mCollisionLayer = ((PageFragment)frag).getCollisionLayer();
        Log.e(debugTag+getTag(), " ** ! ** ! ** ! ** ! ** ! ** ! ** ! mCollisionLayer: " + mCollisionLayer);
        mToolLayer = ((PageFragment)frag).getToolLayer();
        Log.e(debugTag+getTag(), " ** ! ** ! ** ! ** ! ** ! ** ! ** ! mToolLayer: " + mToolLayer);
    }

    // Tool menu selection passed from MainActivity
    // param View v is actual tool view used to start a new drag
    public void onToolSelected(View v) { // ToolMenu selection interface
        if (v != null) {
            Log.e(debugTag+getTag(), " ** ! ** ! ** ! onToolSelected view: " + v);
            initDragVariables();
            ClipData data = ClipData.newPlainText("MENU", "");
            // set viewmodel here before the tool view become visible
            Tool.setUpWithViewModel(mViewModel, (Tool)v);

            // need to assign *this* view to mDragView on touch event so it do
            // not include itself in rectangles collection before calling startDrag
            // Also instantiate a new collision rect for collision layer clip bound.
            mDragView = v;
            mCollisionRect = new Rect();
            mDragView.getDrawingRect(mCollisionRect);
            mCollisionRect.offset(mDragView.getLeft(), mDragView.getTop());
            mCollisionLayer.setClipBounds(mCollisionRect);
            collectRectangles();

            setClickPointOffset(v.getWidth()/2, v.getHeight()/2);
            View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);
//            collectRectangles();
            v.startDrag(data, dragShadowBuilder, v, 0);
            v.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (isEditMode) {
            initDragVariables();
            Log.e(debugTag+getTag(), " ** ! ** ! ** ! onTouch: ToolView Touched!" + v);
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.e(debugTag, "onTouch action_down on view: " + v.getClass().getSimpleName());

                // need to assign *this* view to mDragView on touch event so it do
                // not include itself in rectangles collection before calling startDrag
                // Also instantiate a new collision rect for collision layer clip bound.
                mDragView = v;
                mCollisionRect = new Rect();
                mDragView.getDrawingRect(mCollisionRect);
                mCollisionRect.offset(mDragView.getLeft(), mDragView.getTop());
                mCollisionLayer.setClipBounds(mCollisionRect);
                collectRectangles();

                float x = event.getX();
                float y = event.getY();

                ClipData data = ClipData.newPlainText("", "");
                setClickPointOffset((int)x, (int)y);
                Log.e(debugTag, "VIEW OFFSET: " + mClickPointOffset.x + " " + mClickPointOffset.y );
                DragShadow dragShadowBuilder = new DragShadow(v, mClickPointOffset);
                v.startDrag(data, dragShadowBuilder, v, 0);
                v.setVisibility(View.INVISIBLE);
                getView().invalidate();
                return true;
            } else {
                return false;
            }
        } else return false;
    }

    // Need to extend DragShadowBuilder to provide a constructor taking
    // into account user click point offset when dragging from the workspace.
    private static class DragShadow extends View.DragShadowBuilder {

        private final Point touch;

        public DragShadow(View view, Point touch) {
            super(view);
            this.touch = touch;
        }

        @Override
        public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {
            View v = getView();
            int height = v.getHeight();
            int width = v.getWidth();
            outShadowSize.set(width, height);
            outShadowTouchPoint.set(touch.x, touch.y);
        }
    }


//    // Workspace page fragment adapter. This adapter provide circular paging
//    public class PageFragmentAdapter extends FragmentStateAdapter {
//        FragmentManager fm;
//
//        public PageFragmentAdapter(FragmentManager fm, Lifecycle lc) {
//            super(fm, lc);
//            this.fm = fm;
//        }
//
//        @NonNull
//        @Override
//        public Fragment createFragment(int position) {
//            // Return a NEW fragment instance in createFragment(int)
//            Fragment fragment = PageFragment.newInstance((position% mNumPages));
//            Log.e("PageFragmentAdapter" + getTag(), "createFragment() (position%mNumPages): " + ((position% mNumPages)));
//            fragment.setRetainInstance(true);
//
//            if (!mFragmentList.contains(fragment)) {
//                mFragmentList.set(position % mNumPages, fragment);
//                Log.e("PageFragmentAdapter" + getTag(), "createFragment()fragmentList set at: " + position % mNumPages + " " + fragment);
//                Log.e("PageFragmentAdapter"+ getTag(), "createFragment()fragmentList post size: " + mFragmentList.size() + " " + mFragmentList);
//            }
//            return fragment;
//        }
//
//        public Fragment getFragment(int realposition) {
//            Log.e("PageFragmentAdapter" + getTag(), "getFragment(int realposition) modposition: " + realposition%mNumPages);
//            Log.e("PageFragmentAdapter" + getTag(), "getFragment(int realposition) mFragmentList: " + mFragmentList);
//            return mFragmentList.get(realposition%mNumPages);
//        }
//
//        @Override
//        public int getItemCount() {
//            return Integer.MAX_VALUE;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return (long)(position % mNumPages);
//        }
//
//        @Override
//        public boolean containsItem(long itemId) {
////            return super.containsItem(itemId);
//            return true;
//        }
//
//        public int getCenterPage() {
//            // Offset to make center page to fall back right at BigInt/2
//            int offset = mNumPages == 5 ? 1 : 2;
//            return (Integer.MAX_VALUE / 2)-offset;
//        }
//    }

    // Workspace page fragment adapter. This adapter provide standard paging
    public class PageFragmentAdapter extends FragmentStateAdapter {
        FragmentManager fm;

        public PageFragmentAdapter(FragmentManager fm, Lifecycle lc) {
            super(fm, lc);
            this.fm = fm;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            // Return a NEW fragment instance in createFragment(int)
            Fragment fragment = PageFragment.newInstance(position);
            Log.e("PageFragmentAdapter" + getTag(), "createFragment() (position): " + position);
            fragment.setRetainInstance(true);

            if (!mFragmentList.contains(fragment)) {
                mFragmentList.set(position, fragment);
                Log.e("PageFragmentAdapter" + getTag(), "createFragment()fragmentList set at: " + position + " " + fragment);
                Log.e("PageFragmentAdapter"+ getTag(), "createFragment()fragmentList post size: " + mFragmentList.size() + " " + mFragmentList);
            }
            return fragment;
        }

        public Fragment getFragment(int position) {
            Log.e("PageFragmentAdapter" + getTag(), "getFragment(int position) position: " + position);
            Log.e("PageFragmentAdapter" + getTag(), "getFragment(int position) mFragmentList: " + mFragmentList);
            return mFragmentList.get(position);
        }

        @Override
        public int getItemCount() {
            return mNumPages;
        }

        public int getCenterPage() {
            int center = mNumPages == 5 ? 2 : 1;
            return center;
        }
    }


    // Helper method for rectangles collection in edit mode from onLongClick and onPageSelected
    public void collectRectangles() {
        mRectangles.clear();

        for (int i=0; i<mToolLayer.getChildCount(); i++) {
            if ( mDragView != mToolLayer.getChildAt(i)) {
                Rect r = new Rect();
                View child = mToolLayer.getChildAt(i);
                child.getDrawingRect(r);
                r.offsetTo(child.getLeft(), child.getTop());
                mRectangles.add(r);
                Log.e(debugTag+getTag(), "|||||collectRectangles|||||rect at position: " + r.left + " " + r.top + " added");
                Log.e(debugTag+getTag(), "|||||collectRectangles|||||mRectangles size: " + mRectangles.size());

            }
        }
    }

    // Helper method to trigger timed page change while dragview is in page change zone.
    // This version is non-modulo specific; it will not handle cycling
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {

            int nextPos = mCurPosition + pageChangeStepValue;
            Log.e("DragDrop in Runnable", "nextPos: " + nextPos);
            if (nextPos >= 0 && nextPos < mNumPages) {
                Log.e("DragDrop in Runnable", "nextPos is valid pos");
                // Clear background to prevent artifacts
                PageFragment f = (PageFragment) mPageFragmentAdapter.getFragment(mCurPosition);
                if (f.getCollisionLayer().getBackground() != null) {
                    f.getCollisionLayer().setBackground(null);
                    f.getCollisionLayer().invalidate();
                }

                mCurPosition = nextPos;
                mViewPager.setCurrentItem(mCurPosition, true);

                // Redo every 666ms if still dragging
                if (mDragView != null) {
                    mHandler.postDelayed(this, 666);
                }
            }
        }
    };


}

