package com.example.androfone;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;


public class SelectionControlView extends Tool {

    private static final String DEBUG_TAG = "SelectionControlView";


    public interface SelectionControlListener {
        public void onSelectionStartChanged(float x);
        public void onSelectionEndChanged(float x);
    }

    private Paint mStrokePaint, mFillPaint, mCursorPaint, mCursorFillPaint, mMaskPaint;
    private Rect drawRect;
    private Picture mCachedView;
    private Bitmap mCachedViewBitmap;
    private Picture mCachedCursor;
    private Bitmap mCachedCursorBitmap;
    private Region mRegion;
    private float mDensity;
    private int width;
    private int height;
    private int margin;
    private int offset;
    private int mCursorOffset;
    private int mCursorOffsetY;
    private int cursorHeight;
    private int cursorWidth;
    private Point cursorPos;
    private int lastPosX;
    private int lastPosY;
    private float triHeight;
    private float halfRange;
    private float centerX;
    private SelectionControlListener mListener;
    private WorkspaceViewModel mViewModel;
    private DisplayMetrics mDisplayMetrics;
    private int mScaledUnit;
    private boolean isInteractionEnabled;

    public SelectionControlView(Context context) {
        super(context);
        init(context, null);
    }

    public SelectionControlView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // Convenience constructor
    public SelectionControlView(Context context, WorkspaceViewModel viewModel) {
        super(context, viewModel);
        mViewModel = viewModel;
        init(context, null);
    }

    @Override
    protected void setUpWithViewModelInternal(WorkspaceViewModel viewModel) {
        mViewModel = viewModel;
        mViewModel.getEditMode().observe((LifecycleOwner) getContext(), aBoolean -> {
            if (aBoolean) {
                setBackgroundColor(getResources().getColor(R.color.colorgray));
                setAlpha(0.6f);
                isInteractionEnabled = false; // User do not interact with the view, drag only
            }
            else {
                setBackground(null);
                setAlpha(1);
                isInteractionEnabled = true; // User interact with the view, no drag
            }
        });
    }

    @Override
    protected void init(Context context, AttributeSet attrs) {

        int mStrokeColor = ContextCompat.getColor(context, R.color.default_selectioncontrol_border);
        int mFillColor = ContextCompat.getColor(context, R.color.default_selectioncontrol_fill);
        int mCursorColor = ContextCompat.getColor(context, R.color.default_selectioncontrol_cursor);
        int mCursorFillColor = ContextCompat.getColor(context, R.color.default_selectioncontrol_cursor_fill);
        // debug mask color
        int mMaskColor = ContextCompat.getColor(context, R.color.default_mask);

        float border = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursor = getResources().getDimensionPixelSize(R.dimen.cursor_size);

        mDisplayMetrics = getResources().getDisplayMetrics();

        mStrokePaint = new Paint();
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeJoin(Paint.Join.ROUND);
        mStrokePaint.setStrokeWidth(border);
        mStrokePaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mCursorPaint = new Paint();
        mCursorPaint.setStyle(Paint.Style.STROKE);
        mCursorPaint.setStrokeWidth(cursor);
        mCursorPaint.setStrokeJoin(Paint.Join.ROUND);
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(mCursorColor);

        mCursorFillPaint = new Paint();
        mCursorFillPaint.setStyle(Paint.Style.FILL);
        mCursorFillPaint.setAntiAlias(true);
        mCursorFillPaint.setColor(mCursorFillColor);

        mMaskPaint = new Paint();
        mMaskPaint.setStyle(Paint.Style.FILL);
        mMaskPaint.setColor(mMaskColor);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isInteractionEnabled) {
            // Let super (Tool) handle requestDisallowInterceptTouchEvent(true/false) on parent
            super.onTouchEvent(event);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                if (mViewModel != null && isInteractionEnabled) mViewModel.setIsInteracting(true);
//                getParent().requestDisallowInterceptTouchEvent(isInteractionEnabled);
                    float x = event.getX();
                    float y = event.getY();
//                int range =  (int)((y / triHeight) * halfRange);
                    if (!mRegion.contains((int) x, (int) y)) return false;

                    break;
                case MotionEvent.ACTION_MOVE:
//                mListener.waveformTouchMove(event.getX());
                    x = event.getX();
                    y = event.getY() - cursorHeight;
                    //               float yy = y-cursorHeight; // need to offset y for min/max and factor

                    y = Math.min(triHeight - cursorHeight, Math.max(0, y));
//                Log.d(DEBUG_TAG,"y min - max: " + y);
                    float yfactor = (y) / (triHeight - cursorHeight);
                    float range = yfactor * halfRange;
                    float cx = Math.min(centerX + range, Math.max(centerX - range, x));
//                Log.d(DEBUG_TAG,"x: " + cx + " y: " + y + " range: " + range + " yfactor: " + yfactor);

                    float xfactor = 1 - Math.min(0.98f, yfactor);
                    // cx min = 68, cx max = 652, diff(offset cx) = 68
                    float xCurRangePos = (cx - 68) / 584; // hack; actual values of cx hard coded
//                Log.d(DEBUG_TAG,"xfactor: " + xfactor + " xCurRangePos" + xCurRangePos );
                    float xRange = xfactor * halfRange;
                    float xmin = cx - xRange - 68;
                    float xmax = cx + xRange - 68;
                    float outMin = xmin / 584;
                    float outMax = xmax / 584;
//                Log.d(DEBUG_TAG,"OUTPUT outMin: " + outMin + " outMax: " + outMax );

                    // position cursor
                    cursorPos.set((int) cx, (int) y + cursorHeight); // remove y offset

                    if (mListener != null) {
                        mListener.onSelectionStartChanged(outMin);
                        mListener.onSelectionEndChanged(outMax);
                    }

                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
//                if (mViewModel != null && isInteractionEnabled) mViewModel.setIsInteracting(false);
//                getParent().requestDisallowInterceptTouchEvent(false);
//                mListener.waveformTouchEnd();
                    break;
            }
        }
        return isInteractionEnabled;
    }

    public void setListener(SelectionControlListener listener) {
        mListener = listener;
    }

    public void setCursorPosition(float x, float y) {
        float h = ((1 - y) * 1.02f) * (triHeight-cursorHeight);
        float pos = (x * 2) - 1;
        cursorPos.set((int)(x * (halfRange+halfRange))+ cursorWidth, (int)h + cursorHeight);
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.loop_grid_width) * unit);
        int h = (int) (getResources().getInteger(R.integer.loop_grid_height) * unit);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
        int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // width should be 32 * 10 = 320, height should be 32 * 8 = 256

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        drawRect = new Rect(0, 0, width, height);
        mRegion = new Region();
        createView();
        createCursor();
        // initial cursor pos in top of parent triangle
        cursorPos = new Point((int)centerX, cursorHeight);

        if (mViewModel != null) {
            setUpWithViewModelInternal(mViewModel);
        }
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mCachedView != null && mCachedCursor != null) {
            canvas.drawPicture(mCachedView);
            canvas.translate(cursorPos.x, cursorPos.y);
            canvas.drawPicture(mCachedCursor);
        } else if (mCachedViewBitmap != null && mCachedCursorBitmap != null) {
            canvas.drawBitmap(mCachedViewBitmap, null, drawRect, null);
            canvas.drawBitmap(mCachedCursorBitmap, null, drawRect, null);
        }
    }

    private void createView() {
        if (width <= 0 || height <= 0)
            return;

        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedView = new Picture();
            cacheCanvas = mCachedView.beginRecording(width, height);
        } else {
            mCachedViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedViewBitmap);
        }


        offset = 32;

        int xRange = width - (offset*2); // width - 64 = 256
        int yRange = height - (offset*2); // height - 64 = 192
        halfRange = xRange / 2;
        triHeight = yRange;
        Path path = new Path();
        centerX = width / 2f;

        path.moveTo(offset, height - offset);
        path.lineTo(width - offset, height - offset);
        path.lineTo(centerX, offset);
        path.close();
        mRegion.setPath(path, new Region(drawRect));


//        cacheCanvas.drawColor(mMaskPaint.getColor());
        cacheCanvas.drawPath(path, mFillPaint);
        cacheCanvas.drawPath(path, mStrokePaint);

        if (mCachedView != null)
            mCachedView.endRecording();
    }

    private void createCursor() {
        if (width <= 0 || height <= 0)
            return;

        cursorWidth = width / 10;
        cursorHeight = height / 10;
        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedCursor = new Picture();
            cacheCanvas = mCachedCursor.beginRecording(cursorWidth, cursorHeight);
        } else {
            mCachedCursorBitmap = Bitmap.createBitmap(cursorWidth, cursorHeight, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedCursorBitmap);
        }

        // draw in top of parent triangle
        // parent triangle top offset and stroke width added to height
        int offset = 0; //16 + 12;
        Path path = new Path();
        float offsetX = cursorWidth / 2;
        mCursorOffsetY = cursorHeight / 2;
        mCursorOffset = (int)offsetX;
        halfRange = halfRange - offsetX; // refine halfRange to include cursor halfwidth into account

        path.moveTo( -offsetX, mCursorOffsetY);
        path.lineTo(offsetX, mCursorOffsetY);
        path.lineTo(0, -mCursorOffsetY);
        path.close();

        cacheCanvas.drawPath(path, mCursorFillPaint);
        cacheCanvas.drawPath(path, mCursorPaint);

        if (mCachedCursor != null)
            mCachedCursor.endRecording();
    }
}
