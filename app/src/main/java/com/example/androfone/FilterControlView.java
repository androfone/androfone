package com.example.androfone;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

public class FilterControlView extends Tool {
    private static final String DEBUG_TAG = "FilterControlView";


    public interface FilterControlListener {
        public void onFilterCutoffChanged(float x);
        public void onFilterResonanceChanged(float x);
    }

    private Paint mStrokePaint, mFillPaint, mCursorPaint, mCursorFillPaint, mMarkerPaint, mMaskPaint;
    private Rect drawRect;
    private Picture mCachedView;
    private Bitmap mCachedViewBitmap;
    private Picture mCachedCursor;
    private Bitmap mCachedCursorBitmap;
    private Path mMarker;

    private Rect mClipRect;
    private Region mRegion;
    private Path cursorPath;
    private Path fillPath;
    private float mDensity;
    private int width;
    private int height;

    private int offset;
    private int mCursorOffset;
    private int mCursorOffsetY;
    private int cursorHeight;
    private int cursorWidth;
    private int xRange;
    private int yRange;
    private int yMin;
    private int yMax;
    private float mCursorHalfWidth;
    private float mCursorHalfHeight;
    private float mCursorCornerRadius;
    private DisplayMetrics mDisplayMetrics;
    private int mScaledUnit;
    private boolean isInteractionEnabled;

    private WorkspaceViewModel mViewModel;

    private float mLastX;
    private float mLastY;


    private float centerX;
    private float centerY;
    private com.example.androfone.FilterControlView.FilterControlListener mListener;

    public FilterControlView(Context context) {
        super(context);
        init(context, null);
    }

    public FilterControlView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // Convenience constructor
    public FilterControlView(Context context, WorkspaceViewModel viewModel) {
        super(context);
        mViewModel = viewModel;
        init(context, null);
    }

    @Override
    protected void setUpWithViewModelInternal(WorkspaceViewModel viewModel) {
//        Log.e("YES Where I Want To Be", "setUpWithViewModelInternal");
        mViewModel = viewModel;
//        mViewModel.getFilterCutoff().observe((LifecycleOwner) getContext(), aFloat -> {
//            mLastX = aFloat;
//            positionCursor((int)mLastX, (int)mLastY);
//        });
//        mViewModel.getFilterResonance().observe((LifecycleOwner) getContext(), aFloat -> {
//            mLastY = aFloat;
//            positionCursor((int)mLastX, (int)mLastY);
//        });
        mViewModel.getFilterCursorPosition().observe((LifecycleOwner) getContext(), aPoint -> {
            mLastX = aPoint.x;
            mLastY = aPoint.y;
            repositionCursor((int)mLastX, (int)mLastY);
        });
        mViewModel.getEditMode().observe((LifecycleOwner) getContext(), aBoolean -> {
            if (aBoolean) {
                Log.e("FILTER", "mViewModel.getEditMode() aBoolean should be true: " + aBoolean);
                setBackgroundColor(getResources().getColor(R.color.colorgray));
                setAlpha(0.6f);
                isInteractionEnabled = false; // User do not interact with the view, drag only
            }
            else {
                Log.e("FILTER", "mViewModel.getEditMode() aBoolean should be false: " + aBoolean);
                setBackground(null);
                setAlpha(1);
                isInteractionEnabled = true; // User interact with the view, no drag
            }
        });
    }


    @Override
    protected void init(Context context, AttributeSet attrs) {

        int mStrokeColor = ContextCompat.getColor(context, R.color.default_filter_control_color);
        int mFillColor = ContextCompat.getColor(context, R.color.default_filter_control_background);
        int mCursorColor = ContextCompat.getColor(context, R.color.default_selection);
        // debug mask color
        int mMaskColor = ContextCompat.getColor(context, R.color.default_mask);

        float border = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursor = getResources().getDimensionPixelSize(R.dimen.cursor_size);
        float medium = getResources().getDimensionPixelSize(R.dimen.medium_size);
        float small = getResources().getDimensionPixelSize(R.dimen.small_size);
        float thin = getResources().getDimensionPixelSize(R.dimen.thin_size);
        float hairline = getResources().getDimensionPixelSize(R.dimen.hairline_size);
        float six = 6, four = 4, three = 3, two = 2, one = 1, half = 0.5f;

//        mCursorHalfHeight = getResources().getDimension()
        mDisplayMetrics = getResources().getDisplayMetrics();
        mCursorHalfHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, mDisplayMetrics);
        mCursorCornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4.5f, mDisplayMetrics);


        mStrokePaint = new Paint();
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeJoin(Paint.Join.ROUND);
        mStrokePaint.setStrokeWidth(border);
        mStrokePaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mCursorPaint = new Paint();
        mCursorPaint.setStyle(Paint.Style.STROKE);
        mCursorPaint.setStrokeWidth(cursor);
        mCursorPaint.setStrokeJoin(Paint.Join.ROUND);
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(mCursorColor);


        mCursorFillPaint = new Paint();
        mCursorFillPaint.setStyle(Paint.Style.FILL);
        mCursorFillPaint.setAntiAlias(true);
        mCursorFillPaint.setColor(mStrokeColor);

        mMarkerPaint = new Paint();
        mMarkerPaint.setColor(mStrokeColor);
        mMarkerPaint.setStyle(Paint.Style.STROKE);
        mMarkerPaint.setStrokeWidth(border);
        mMarkerPaint.setAntiAlias(true);


        mMaskPaint = new Paint();
        mMaskPaint.setStyle(Paint.Style.FILL);
        mMaskPaint.setColor(mMaskColor);
    }

    public void setListener(FilterControlListener listener) {
        mListener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.filter_grid_width) * unit);
        int h = (int) (getResources().getInteger(R.integer.filter_grid_height) * unit);
        int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
        int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        drawRect = new Rect(0, 0, width, height);
        mRegion = new Region();
        createView();
        createCursor();

        if (mViewModel != null) {
            setUpWithViewModelInternal(mViewModel);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isInteractionEnabled) {

            super.onTouchEvent(event);
            Log.d(DEBUG_TAG, "!!!   !!!   !!!!   TOUCH!   !!!   !!!!   !!!!");
            float xmin = offset;
            float xmax = width - xmin;
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    x = Math.min(xmax, Math.max(xmin, x));
                    y = Math.min(yMax, Math.max(yMin, y));

                    float factor = ((y - yMin) / yRange);
                    // range = (halfrange-halfcursor) + halfcursor
                    mCursorHalfWidth = (factor * ((xRange / 2) - (offset / 2))) + (offset / 2);


                    float cutoff = (x - offset) / xRange;
                    Log.d(DEBUG_TAG, "factor " + factor + " cutoff " + cutoff);

                    mLastX = x;
                    mLastY = y;

                    positionCursor((int) x, (int) y);

                    if (mListener != null) {
                        mListener.onFilterCutoffChanged(cutoff);
                        mListener.onFilterResonanceChanged(factor);
                    }

                    if (mViewModel != null) {
                        mViewModel.setFilterCutoff(x);
                        mViewModel.setFilterResonance(y);
                        mViewModel.setFilterCursorPosition(new Point((int) x, (int) y));
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
        }
        return isInteractionEnabled;
    }

    private void repositionCursor(int x, int y) {
        float factor = (((float)y - yMin) / yRange);
        Log.d(DEBUG_TAG, "repositionCursor(int x, int y) y, factor: " + y +" "+ factor);
        mCursorHalfWidth = (factor * ((xRange/2)-(offset/2))) + (offset/2);
        Log.d(DEBUG_TAG, "repositionCursor(int x, int y) mCursorHalfWidth: " + mCursorHalfWidth);
        positionCursor(x, y);
    }

    void positionCursor(int x, int y) {
        Canvas canvas;
        canvas = mCachedCursor.beginRecording(width, height);
        cursorPath.reset();
//        cursorPath.addRoundRect(new RectF(-halfWidth, -16, halfWidth, 16),
//                10.0f, 10.0f, Path.Direction.CCW );
        cursorPath.addRoundRect(new RectF(-mCursorHalfWidth, -8, mCursorHalfWidth, 8),
                10.0f, 10.0f, Path.Direction.CCW );

        cursorPath.close();
        canvas.translate(x, y);
        canvas.drawPath(cursorPath, mCursorFillPaint);
        canvas.drawPath(cursorPath, mCursorPaint);

        if (mCachedCursor != null)
            mCachedCursor.endRecording();
        invalidate();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if ((mCachedView != null) && (mCachedCursor != null)) {
            canvas.drawPicture(mCachedView);
            canvas.clipRect(mClipRect);
            canvas.drawPicture(mCachedCursor);
            canvas.drawPath(mMarker, mMarkerPaint);
        } else if (mCachedViewBitmap != null && mCachedCursorBitmap != null) {
            canvas.drawBitmap(mCachedViewBitmap, null, drawRect, null);
            canvas.drawBitmap(mCachedCursorBitmap, null, drawRect, null);
        }
    }

    private void createView() {
        if (width <= 0 || height <= 0)
            return;

        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedView = new Picture();
            cacheCanvas = mCachedView.beginRecording(width, height);
        } else {
            mCachedViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedViewBitmap);
        }

//        offset = 16; //32;
        offset = mScaledUnit/2;
        centerX = width / 2;
        centerY = height / 2;
        int margin = (offset/2) + 6; //12; // 16 + strokewidth
        int xmin = offset;
        int xmax = width - offset;
        xRange = xmax - xmin;
        yMin = offset + 4; // offset + "cursor_size" stroke width (4);
        yMax = height - yMin;
        yRange = yMax - yMin;

        // x inside range minus cursor min width
        mCursorHalfWidth = (xRange/ 2) - (offset*2); // will be scaled by y 0 - 1

        Path path = new Path();
        mClipRect = new Rect(margin, margin, width-margin, height-margin);

        RectF r = new RectF(offset, offset, width - offset, height - offset);
        path.addRect(r, Path.Direction.CCW);
        path.close();

        //       cacheCanvas.drawColor(mMaskPaint.getColor());
        cacheCanvas.drawPath(path, mStrokePaint);
        cacheCanvas.drawPath(path, mFillPaint);


        // create marker
        mMarker = new Path();
        mMarker.moveTo(centerX, height-offset);
        mMarker.lineTo(centerX, height-(offset*2));

        if (mCachedView != null)
            mCachedView.endRecording();
        invalidate();
    }

    private void createCursor() {
        if (width <= 0 || height <= 0)
            return;

        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedCursor = new Picture();
            cacheCanvas = mCachedCursor.beginRecording(width, height);
        } else {
            mCachedCursorBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedCursorBitmap);
        }

        Path path = new Path();
        // initial position full width bottom - position is position - x center
        RectF r = new RectF(-(xRange/2), yMax-offset, (xRange/2), yMax);
        path.addRoundRect( r, 10.0f, 10.0f, Path.Direction.CCW);
        path.close();

        cacheCanvas.translate(centerX, 0);
        cacheCanvas.drawPath(path, mCursorFillPaint);
        cacheCanvas.drawPath(path, mCursorPaint);
        cursorPath = path;

        if (mCachedCursor != null)
            mCachedCursor.endRecording();
        invalidate();
    }

}
