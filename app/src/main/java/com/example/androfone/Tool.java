package com.example.androfone;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModel;

import org.jetbrains.annotations.NotNull;

public class Tool extends View implements LifecycleOwner {

    private final LifecycleRegistry registry = new LifecycleRegistry(this);

    public Tool(Context context) {
        super(context);
        init(context, null);
    }

    public Tool(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // Convenience constructor
    public Tool(Context context, WorkspaceViewModel viewModel) {
        super(context);
    }


    protected void init(Context context, AttributeSet attrs) {

    }

    public static void setUpWithViewModel(WorkspaceViewModel viewModel, @NotNull Tool tool) {
        tool.setUpWithViewModelInternal(viewModel);
    }

    protected void setUpWithViewModelInternal(WorkspaceViewModel viewModel) {
        // override this method in your custom tool to implement internal livedata connections
    }

    // This will overcome page flipping while interacting with any tool
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
                getParent().requestDisallowInterceptTouchEvent(false);
                break;
        }
        return true;
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return registry;
    }
}
